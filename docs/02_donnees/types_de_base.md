---
author: Steeve PYTEL
title: 📋 Types de base
---

!!! abstract "Présentation"

    Toute machine informatique manipule une représentation des données dont l’unité minimale est le bit 0/1, ce qui permet d’unifier logique et calcul. Les données de base sont représentées selon un codage dépendant de leur nature : entiers, flottants, caractères et chaînes de caractères. Le codage conditionne la taille des différentes valeurs en mémoire. 

|Contenus|Capacités attendues|
|:--- | :--- |
|Écriture d’un entier positif dans une base b ⩾ 2 |Passer de la représentation d’une base dans une autre.|
|Représentation binaire d’un entier relatif|Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entiers. Utiliser le complément à 2.|
|Représentation approximative des nombres réels : notion de nombre flottant |Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3.|
|Valeurs booléennes : 0,1. Opérateurs booléens : and, or, not.Expressions booléennes|Dresser la table d’une expression booléenne.|
|Représentation d’un texte en machine. Exemples des encodages ASCII, ISO-8859-1, Unicode|Identifier l’intérêt des différents systèmes d’encodage. Convertir un fichier texte dans différents formats d’encodage.|