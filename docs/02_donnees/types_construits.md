---
author: Steeve PYTEL
title: 📋 Types construits
---

!!! abstract "Présentation"

    À partir des types de base se constituent des types construits, qui sont introduits au fur et à mesure qu’ils sont nécessaires.     
    Il s’agit de présenter tour à tour les p-uplets (tuples), les enregistrements qui collectent des valeurs de types différents dans des champs nommés et les tableaux qui permettent un accès calculé direct aux éléments. En pratique, on utilise les  appellations de Python, qui peuvent être différentes de celles d’autres langages de programmation. 

|Contenus|Capacités attendues|
|:--- | :--- |
|p-uplets.  p-uplets nommés|Écrire une fonction renvoyant un p-uplet de valeurs.|
|Tableau indexé, tableau donné en compréhension|Lire et modifier les éléments d’un tableau grâce à leurs index. Construire un tableau par compréhension. Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j]. Itérer sur les éléments d’un tableau. |
|Dictionnaires par clés et valeurs  |Construire une entrée de dictionnaire. Itérer sur les éléments d’un dictionnaire.|
