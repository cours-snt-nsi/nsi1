---
author: Steeve PYTEL
title: 📋 CAPYTALE
---

# Exercices

??? alerte "Programmation"

    - [NSI Première Partie 1 Chapitre 1 Prise en main Notebook Jupyter 1/2](https://capytale2.ac-paris.fr/web/c/be5a-3800776){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 1 Prise en main Notebook Jupyter 2/2](https://capytale2.ac-paris.fr/web/c/cd56-3800777){:target="_blank"}


    - [NSI Première Partie 1 Chapitre 2 Variables et affectation](https://capytale2.ac-paris.fr/web/c/b74b-3801176){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 2 Types](https://capytale2.ac-paris.fr/web/c/bee1-3801174){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 3 Fonctions introduction](https://capytale2.ac-paris.fr/web/c/1f89-3801223){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 3 Fonctions partie 1](https://capytale2.ac-paris.fr/web/c/615c-3801224){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 3 Fonctions partie 2](https://capytale2.ac-paris.fr/web/c/9bb5-3801225){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 4 Conditions et tests](https://capytale2.ac-paris.fr/web/c/8b20-3801226){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 4 Embranchements simples](https://capytale2.ac-paris.fr/web/c/354f-3801228){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 4 Embranchements multiples](https://capytale2.ac-paris.fr/web/c/2420-3801227){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 5 Boucles itératives](https://capytale2.ac-paris.fr/web/c/3ce8-3801230){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 5 Boucles conditionnelles](https://capytale2.ac-paris.fr/web/c/e9d9-3801229){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 6 Listes introduction](https://capytale2.ac-paris.fr/web/c/301d-3801234){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 6 Listes définition et indexation](https://capytale2.ac-paris.fr/web/c/fdc5-3801232){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 6 Listes compréhension](https://capytale2.ac-paris.fr/web/c/3299-3801231){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 6 Listes parcours](https://capytale2.ac-paris.fr/web/c/cc0f-3801236){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 6 Listes fonctions méthodes](https://capytale2.ac-paris.fr/web/c/676c-3801233){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 6 Listes méthode append](https://capytale2.ac-paris.fr/web/c/5a8c-3801235){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 7 Tuples 1/2](https://capytale2.ac-paris.fr/web/c/8969-3801237){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 7 Tuples 2/2](https://capytale2.ac-paris.fr/web/c/6866-3801238){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 8 Textes](https://capytale2.ac-paris.fr/web/c/1756-3801240){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 8 Fichiers](https://capytale2.ac-paris.fr/web/c/54e7-3801239){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 9 Dictionnaires](https://capytale2.ac-paris.fr/web/c/c459-3801243){:target="_blank"}

    - [NSI Première Partie 1 Chapitre 10 Données en table 1/2](https://capytale2.ac-paris.fr/web/c/7248-3801244){:target="_blank"}
    - [NSI Première Partie 1 Chapitre 10 Données en table 2/2](https://capytale2.ac-paris.fr/web/c/6196-3801245){:target="_blank"}

??? alerte "Données"

    - [NSI Première Partie 2 Chapitre 2 Codage entiers naturels](https://capytale2.ac-paris.fr/web/c/900b-3801250){:target="_blank"}

??? alerte "Architecture"

    - rien pour le moment ^^

??? alerte "Algorithmique"

    - [NSI Première Partie 4 Chapitre 1 Spécification et tests](https://capytale2.ac-paris.fr/web/c/90b2-3801252){:target="_blank"}

    - [NSI Première Partie 4 Chapitre 4 Tri sélection](https://capytale2.ac-paris.fr/web/c/9ae1-3801254){:target="_blank"}
    - [NSI Première Partie 4 Chapitre 4 Tri insertion](https://capytale2.ac-paris.fr/web/c/7a9a-3801253){:target="_blank"}

    - [NSI Première Partie 4 Chapitre 6 kNN](https://capytale2.ac-paris.fr/web/c/a2ce-3801255){:target="_blank"}

??? alerte "Réseaux"

    - rien pour le moment ^^

??? alerte "IHM_Web"

    - rien pour le moment ^^

??? abstract "Débuter avec les Outils en ligne"

    1. [Boucles](https://capytale2.ac-paris.fr/web/c/073b-742357){target='_blank'}
    2. [Si](https://capytale2.ac-paris.fr/web/c/6961-922493){target='_blank'}  
    3. [While](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    4. [Dessin](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    5. [Mélange](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    6. [Découverte Python](https://capytale2.ac-paris.fr/web/c/ca10-632362){target='_blank'}

??? abstract "Activitées Pixel'Art"

    1. [Easy](https://capytale2.ac-paris.fr/web/c/dbee-644319){target='_blank'}
    2. [Drapeau](https://capytale2.ac-paris.fr/web/c/125f-1447786){target='_blank'}
    3. [Zig](https://capytale2.ac-paris.fr/web/c/4cc1-567778){target='_blank'}
    4. [Echec](https://capytale2.ac-paris.fr/web/c/ab28-802220){target='_blank'}
   
  
??? abstract "Le web"

    1. [page vide](https://capytale2.ac-paris.fr/web/c/910d-980620){target='_blank'}
    2. [Jeu](https://capytale2.ac-paris.fr/web/c/59e2-2069430){target='_blank'}
    3. [Html 1/2](https://capytale2.ac-paris.fr/web/c/60a6-2549442){target='_blank'}
    4. [Html 2/2](https://capytale2.ac-paris.fr/web/c/fdad-2518372){target='_blank'}
    5. [Pomme Poire](https://capytale2.ac-paris.fr/web/c/ee80-936514){target='_blank'}

??? abstract "Les Objets "

    1. [Microbit](https://capytale2.ac-paris.fr/web/c/c2d5-1767097){target='_blank'}

??? abstract "Les Reseaux Sociaux "

    1. [Graphes 1/2](https://capytale2.ac-paris.fr/web/c/cc08-3406821){target='_blank'}
    2. [Graphes 2/2](https://capytale2.ac-paris.fr/web/c/9dcd-3133354){target='_blank'}

??? abstract "Localisation et Cartographie "

    1. [EXIF 1](https://capytale2.ac-paris.fr/web/c/e0b9-1658411){target='_blank'}
    2. [EXIF 2](https://capytale2.ac-paris.fr/web/c/339d-1658359){target='_blank'}
    3. [EXIF 3](https://capytale2.ac-paris.fr/web/c/23a6-1489513){target='_blank'}

??? abstract "Image"

    1. [Coloriage](https://capytale2.ac-paris.fr/web/c/073e-1952982){target='_blank'}
    2. [Manipulation](https://capytale2.ac-paris.fr/web/c/6271-3574308){target='_blank'}
    3. [Cache-cache](https://capytale2.ac-paris.fr/web/c/9cdd-3700092){target='_blank'}
    4. [Cache-Console](https://capytale2.ac-paris.fr/web/c/6218-1490234){target='_blank'}

### Téléchargement 

[Téléchargement toutes les ressources](../../../documents/resources.zip){:download="resources.zip"}.
