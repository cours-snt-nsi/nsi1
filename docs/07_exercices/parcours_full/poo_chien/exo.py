

# --------- PYODIDE:code --------- #

class Chien:
    def __init__(self, nom, poids):
        self.... = nom
        self.... = poids

    def donne_nom(self):
        return self....

    def ...(self):
        return self....

    def machouille(self, jouet):
        resultat = ""
        for i in range(...):
            resultat += jouet[...]
        return ...

    def ...(self, ...):
        ...

    def ...(self, ration):
        if ...:
            ...
            return True
        else:
            return ...

# --------- PYODIDE:corr --------- #

class Chien:
    def __init__(self, nom, poids):
        self.nom = nom
        self.poids = poids

    def donne_nom(self):
        return self.nom

    def donne_poids(self):
        return self.poids

    def machouille(self, jouet):
        resultat = ""
        for i in range(len(jouet) - 1):
            resultat += jouet[i]
        return resultat

    def aboie(self, nombre):
        return "Ouaf" * nombre

    def mange(self, ration):
        if 0 < ration <= self.poids / 10:
            self.poids += ration
            return True
        else:
            return False

# --------- PYODIDE:tests --------- #

medor = Chien('Médor', 12.0)
assert medor.donne_nom() == 'Médor'
assert medor.donne_poids() == 12.0
assert medor.machouille('bâton') == 'bâto'
assert medor.aboie(3) == 'OuafOuafOuaf'
assert medor.mange(2.0) is False
assert medor.mange(1.0) is True
assert medor.donne_poids() == 13.0
assert medor.mange(1.3) is True

# --------- PYODIDE:secrets --------- #

# Tests publics
medor = Chien("Médor", 12.0)
assert medor.donne_nom() == "Médor"
assert medor.donne_poids() == 12.0
assert medor.machouille("bâton") == "bâto"
assert medor.aboie(3) == "OuafOuafOuaf"
assert medor.mange(2.0) is False
assert medor.mange(1.0) is True
assert medor.donne_poids() == 13.0
assert medor.mange(1.3) is True

# Tests secrets
toutou = Chien("toutou", 10.0)
assert toutou.donne_nom() == "toutou", "Erreur sur la méthode donne_nom"
toutou.nom = "Toutou"
assert toutou.donne_nom() == "Toutou", "Attribut mal nommé"
assert toutou.donne_poids() == 10.0, "Erreur sur la méthode donne_poids"
print("Tests du constructeur passés avec succès")
toutou.poids = 11.0
assert toutou.donne_poids() == 11.0, "Attribut mal nommé"
assert toutou.machouille("balle") == "ball", "Erreur sur la méthode machouille"
assert toutou.machouille("") == "", "Erreur sur la méthode machouille"
print("Tests de la méthode 'machouille' passés avec succès")
assert toutou.aboie(30) == "Ouaf" * 30, "Erreur sur la méthode aboie"
assert toutou.aboie(0) == "", "Erreur sur la méthode aboie"
print("Tests de la méthode 'aboie' passés avec succès")
assert toutou.mange(-1.0) is False, "Erreur sur la méthode mange"
assert toutou.mange(11.0) is False, "Erreur sur la méthode mange"
assert toutou.mange(0.0) is False, "Erreur sur la méthode mange"
assert toutou.mange(1.1) is True, "Erreur sur la méthode mange"
assert toutou.donne_poids() == 12.1, "Erreur sur la méthode mange"
print("Tests de la méthode 'mange' passés avec succès")
