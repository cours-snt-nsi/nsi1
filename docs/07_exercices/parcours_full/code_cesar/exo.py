

# --------- PYODIDE:code --------- #

def indice(caractere):
    "Renvoie l'indice de `caractere` qui doit être une majuscule"
    return ord(caractere) - ord('A')

def majuscule(i):
    """Renvoie la majuscule d'indice donnée
    majuscule(0) renvoie 'A'
    majuscule(25) renvoie 'Z'
    """
    return chr(ord('A') + i)

def cesar(message, decalage):
    resultat = ''
    for caractere in ...:
        if 'A' <= caractere <= ...:
            i = ...(caractere)
            i = (i + ...) ...
            resultat += ...(i)
        else:
            resultat += ...
    return ...

# --------- PYODIDE:corr --------- #

def indice(caractere):
    "Renvoie l'indice de `caractere` qui doit être une majuscule"
    return ord(caractere) - ord("A")


def majuscule(i):
    """Renvoie la majuscule d'indice donnée
    majuscule(0) renvoie 'A'
    majuscule(25) renvoie 'Z'
    """
    return chr(ord("A") + i)


def cesar(message, decalage):
    resultat = ""
    for caractere in message:
        if "A" <= caractere <= "Z":
            i = indice(caractere)
            i = (i + decalage) % 26
            resultat += majuscule(i)
        else:
            resultat += caractere
    return resultat

# --------- PYODIDE:tests --------- #

assert cesar('HELLO WORLD!', 5) == 'MJQQT BTWQI!'
assert cesar('MJQQT BTWQI!', -5) == 'HELLO WORLD!'

assert cesar('BONJOUR LE MONDE !', 23) == 'YLKGLRO IB JLKAB !'
assert cesar('YLKGLRO IB JLKAB !', -23) == 'BONJOUR LE MONDE !'

# --------- PYODIDE:secrets --------- #

# tests
assert cesar("HELLO WORLD!", 5) == "MJQQT BTWQI!"
assert cesar("MJQQT BTWQI!", -5) == "HELLO WORLD!"
assert cesar("BONJOUR LE MONDE !", 23) == "YLKGLRO IB JLKAB !"
assert cesar("YLKGLRO IB JLKAB !", -23) == "BONJOUR LE MONDE !"


# autres tests

assert cesar("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1) == "BCDEFGHIJKLMNOPQRSTUVWXYZA"
assert cesar("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 24) == "YZABCDEFGHIJKLMNOPQRSTUVWX"
assert cesar("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
assert cesar("", 7) == ""
assert cesar("!?.:", 7) == "!?.:"
assert cesar("CESAR", 139) == "LNBJA"
