# tests
assert distance(0, 2, 3, 6) == 5.0
assert distance(10, 4, 5, -8) == 13.0
# tests secrets
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    attendu = sqrt((x_B - x_A) ** 2 + (y_B - y_A) ** 2)
    assert (
        abs(distance(x_A, y_A, x_B, y_B) - attendu) < 10**-6
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"


