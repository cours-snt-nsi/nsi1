Il est aussi de simplifier la fonction en renvoyant directement le résultat de la comparaison :

```python
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    x_AC = x_C - x_A
    y_AC = y_C - y_A
    return x_AB * y_AC - y_AB * x_AC == 0
```
