La définition récursive de la suite pourrait inciter à écrire la fonction avec une fonction récursive.

Cependant, il faut garder à l'esprit que chaque appel de la fonction entraîne deux autres appels à cette même fonction. Ainsi, l'appel de cette fonction pour $N$ termes générera $2^N$ appels, ce qui rend la fonction peu efficace pour de grandes valeurs de $N$, et même susceptible d'atteindre la limite de récursion du langage Python.
