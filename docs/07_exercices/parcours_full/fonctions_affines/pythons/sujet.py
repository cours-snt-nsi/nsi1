# --- hdr, fonction --- #
def g(x):
    pass


# --- exo, fonction --- #
def f(x):
    return 5 * x - 9


# compléter ci-dessous avec votre fonction
...


# --- corr, fonction --- #
def g(x):
    return -3 * x + 7


# --- tests, fonction --- #
# la fonction f n'a pas été modifiée
assert f(0) == -9
assert f(10) == 41
# définition correcte de la fonction g
assert g(0) == 7
assert g(10) == -23


# --- secrets, fonction --- #
def _g(x):
    return -3 * x + 7


for x in range(-10, 11):
    attendu = _g(x)
    assert g(x) == attendu, f"Erreur en calculant g({x})"


# --- exo, zero --- #
def antecedent_zero(a, b):
    ...


# --- corr, zero --- #
def antecedent_zero(a, b):
    return -b / a


# --- tests, zero --- #
assert abs(antecedent_zero(4, -8) - 2) < 1e-6
assert abs(antecedent_zero(-2, 15) - 7.5) < 1e-6
# --- secrets, zero --- #
from random import randrange

for test in range(10):
    a = randrange(-20, 20)
    b = randrange(-50, 50)
    if a == 0:
        continue
    attendu = -b / a
    assert (
        abs(antecedent_zero(a, b) - attendu) < 1e-6
    ), f"Erreur en calculant antecedent_zero{a, b}"


# --- exo, antecedent --- #
def antecedent(a, b, y):
    ...


# --- corr, antecedent --- #
def antecedent(a, b, y):
    return (y - b) / a


# --- tests, antecedent --- #
assert abs(antecedent(4, -8, 5) - 3.25) < 1e-6
assert abs(antecedent(-2, 15, 5) - 5) < 1e-6
# --- secrets, antecedent --- #
from random import randrange

for test in range(10):
    a = randrange(-20, 20)
    b = randrange(-50, 50)
    y = randrange(-500, 500)
    if a == 0:
        continue
    attendu = (y - b) / a
    assert (
        abs(antecedent(a, b, y) - attendu) < 1e-6
    ), f"Erreur en calculant antecedent{a, b, y}"


# --- exo, variations --- #
def variations(a, b):
    ...


# --- corr, variations --- #
def variations(a, b):
    if a > 0:
        return "croissante"
    elif a == 0:
        return "constante"
    else:
        return "décroissante"


# --- tests, variations --- #
assert variations(4, -8) == "croissante"
assert variations(-2, 15) == "décroissante"
# --- secrets, variations --- #
a = 0
b = randrange(-50, 50)
attendu = "constante"
assert variations(a, b) == attendu, f"Erreur en calculant variations{a, b}"

from random import randrange

for test in range(10):
    a = randrange(-20, 20)
    b = randrange(-50, 50)
    attendu = "croissante" if a > 0 else "constante" if a == 0 else "décroissante"
    assert variations(a, b) == attendu, f"Erreur en calculant variations{a, b}"


# --- HDR, coeff_dir --- #
def coefficient_directeur(x_A, y_A, x_B, y_B):
    pass


# --- exo, coeff_dir --- #
def coefficient_directeur(x_A, y_A, x_B, y_B):
    ...


# --- corr, coeff_dir --- #
def coefficient_directeur(x_A, y_A, x_B, y_B):
    delta_y = y_B - y_A
    delta_x = x_B - x_A
    return delta_y / delta_x


# --- tests, coeff_dir --- #
assert abs(coefficient_directeur(1, 4, 9, 10) - 0.75) < 1e-6
assert abs(coefficient_directeur(3, 15, -2, 15) - 0) < 1e-6
# --- secrets, coeff_dir --- #
from random import randrange

for test in range(10):
    x_A = randrange(-50, 50)
    y_A = randrange(-50, 50)
    x_B = randrange(-50, 50)
    y_B = randrange(-50, 50)
    if x_A == x_B:
        continue
    attendu = (y_B - y_A) / (x_B - x_A)
    assert (
        abs(coefficient_directeur(x_A, y_A, x_B, y_B) - attendu) < 1e-6
    ), f"Erreur en calculant coefficient_directeur{x_A, y_A, x_B, y_B}"


# --- HDR, ord_origine --- #
def coefficient_directeur(x_A, y_A, x_B, y_B):
    delta_y = y_B - y_A
    delta_x = x_B - x_A
    return delta_y / delta_x


# --- exo, ord_origine --- #
def ordonnee_origine(x_A, y_A, x_B, y_B):
    ...


# --- corr, ord_origine --- #
def ordonnee_origine(x_A, y_A, x_B, y_B):
    a = coefficient_directeur(x_A, y_A, x_B, y_B)
    return y_A - x_A * a


# --- tests, ord_origine --- #
assert abs(ordonnee_origine(1, 4, 9, 10) - 3.25) < 1e-6
assert abs(ordonnee_origine(3, 15, -2, 15) - 15) < 1e-6
# --- secrets, ord_origine --- #
from random import randrange

for test in range(10):
    x_A = randrange(-50, 50)
    y_A = randrange(-50, 50)
    x_B = randrange(-50, 50)
    y_B = randrange(-50, 50)
    if x_A == x_B:
        continue
    attendu = y_A - x_A * coefficient_directeur(x_A, y_A, x_B, y_B)
    assert (
        abs(ordonnee_origine(x_A, y_A, x_B, y_B) - attendu) < 1e-6
    ), f"Erreur en calculant ordonnee_origine{x_A, y_A, x_B, y_B}"
