

# --------- PYODIDE:code --------- #

def antecedent(a, b, y):
    ...

# --------- PYODIDE:corr --------- #

def antecedent(a, b, y):
    return (y - b) / a

# --------- PYODIDE:tests --------- #

assert abs(antecedent(4, -8, 5) - 3.25) < 1e-6
assert abs(antecedent(-2, 15, 5) - 5) < 1e-6

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for test in range(10):
    a = randrange(-20, 20)
    b = randrange(-50, 50)
    y = randrange(-500, 500)
    if a == 0:
        continue
    attendu = (y - b) / a
    assert (
        abs(antecedent(a, b, y) - attendu) < 1e-6
    ), f"Erreur en calculant antecedent{a, b, y}"