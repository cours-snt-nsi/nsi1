

# --------- PYODIDE:code --------- #

def non(a):
    ...


def et(a, b):
    ...


def ou(a, b):
    ...


def ou_exclusif(a, b):
    ...


def non_ou(a, b):
    ...


def non_et(a, b):
    ...

# --------- PYODIDE:corr --------- #

def non(a):
    if a:
        return False
    else:
        return True


def et(a, b):
    if a:
        if b:
            return True
    return False


def ou(a, b):
    if a:
        return True
    elif b:
        return True
    return False


def ou_exclusif(a, b):
    return ou(et(a, non(b)), et(non(a), b))


def non_ou(a, b):
    return non(ou(a, b))


def non_et(a, b):
    return non(et(a, b))

# --------- PYODIDE:tests --------- #

assert non(True) is False
assert et(True, False) is False
assert ou(True, False) is True
assert ou_exclusif(True, True) is False
assert non_ou(False, True) is False
assert non_et(False, True) is True

# --------- PYODIDE:secrets --------- #


# Test supplémentaires
for a, b in ((False, False), (False, True), (True, False), (True, True)):
    assert non(a) == (not a)
    assert et(a, b) == (a and b)
    assert ou(a, b) == (a or b)
    assert ou_exclusif(a, b) == (a ^ b)
    assert non_ou(a, b) == (not (a or b))
    assert non_et(a, b) == (not (a and b))