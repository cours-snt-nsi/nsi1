👉 Dans la solution proposée, on sort de la boucle :
`#!py while i_a < len(liste_a) and i_b < len(liste_b):`
lorsque `#!py i_a > len(liste_a)` ou `#!py  i_b < len(liste_b)`. C'est-à-dire lorsqu'on a pris toutes les valeurs d'une des deux listes. L'autre liste n'a peut-être pas encore été entièrement parcourue.

Il y a donc au plus une des deux boucles `while` suivantes qui sera exécutée.


👉 Autre solution possible avec une seule boucle `for`:

```python
def fusion(liste_a, liste_b):
    n_a = len(liste_a)
    n_b = len(liste_b)
    i_a = 0
    i_b = 0
    liste_triee = []
    for i in range(n_a+n_b):
        if i_a < n_a and (i_b >= n_b or liste_a[i_a] <= liste_b[i_b]):
            liste_triee.append(liste_a[i_a])
            i_a += 1
        else:
            liste_triee.append(liste_b[i_b])
            i_b += 1
    return liste_triee
```
On rajoute la valeur venant de `liste_a` si :

-   Elle n'a pas été totalement parcourue (`#!py i_a < n_a`)
-   et si une des deux conditions est vérifiée :

    -   la liste `liste_b` a été totalement parcourue (`#!py i_b >= n_b`)
    -   la valeur courante de `liste_a` est inférieure à celle de
        `liste_b` (`#!py liste_a[i_a] <= liste_b[i_b]`)

Dans tous les autres cas, il faut prendre la valeur courante de `liste_b`.
