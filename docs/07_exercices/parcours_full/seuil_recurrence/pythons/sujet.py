# --- exo  --- #
def seuil(precision):
    ...


# --- vide --- #
""" # skip
def seuil(precision):
    n = ...
    u = ...
    while abs(...) > ...:
        n = ...
        u = ...
    return ...
"""  # skip


# --- corr --- #
def seuil(precision):
    n = 0
    u = 1
    while abs(u - 28) > precision:
        n = n + 1
        u = 0.75 * u + 7
    return n
# --- tests --- #
assert seuil(1) == 12    # u_12 ≃ 27,14 et ǀ27,14 - 28ǀ ⩽ 1
assert seuil(0.1) == 20  # u_20 ≃ 27,91 et ǀ27,91 - 28ǀ ⩽ 0,1
# --- secrets --- #
attendus = (
    (1, 12),
    (0.1, 20),
    (0.01, 28),
    (0.001, 36),
    (0.0001, 44),
    (1e-05, 52),
    (1e-06, 60),
    (1e-07, 68),
    (1e-08, 76),
    (1e-09, 84),
    (1e-10, 92),
    (1e-11, 100),
    (1e-12, 108),
    (1e-13, 116),
    (1e-14, 124),
)
for precision, n in attendus:
    assert seuil(precision) == n, f"Erreur avec {precision = }"
