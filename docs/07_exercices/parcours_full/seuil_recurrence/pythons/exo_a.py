

# --------- PYODIDE:code --------- #

def seuil(precision):
    n = ...
    u = ...
    while abs(...) > ...:
        n = ...
        u = ...
    return ...

# --------- PYODIDE:corr --------- #

def seuil(precision):
    n = 0
    u = 1
    while abs(u - 28) > precision:
        n = n + 1
        u = 0.75 * u + 7
    return n

# --------- PYODIDE:tests --------- #

assert seuil(30) == 0  # u_0 = 1 et ǀ1 - 28ǀ ⩽ 30
assert seuil(10) == 4  # u_4 ≃ 19,5 et ǀ19,5 - 28ǀ ⩽ 10
assert seuil(1) == 12  # u_12 ≃ 27,1 et ǀ27,1 - 28ǀ ⩽ 1

# --------- PYODIDE:secrets --------- #


# tests secrets
attendus = (
    (1, 12),
    (0.1, 20),
    (0.01, 28),
    (0.001, 36),
    (0.0001, 44),
    (1e-05, 52),
    (1e-06, 60),
    (1e-07, 68),
    (1e-08, 76),
    (1e-09, 84),
    (1e-10, 92),
    (1e-11, 100),
    (1e-12, 108),
    (1e-13, 116),
    (1e-14, 124),
)
for precision, n in attendus:
    assert seuil(precision) == n, f"Erreur avec {precision = }"