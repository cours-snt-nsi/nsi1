

# --------- PYODIDE:code --------- #

def matrice_vers_liste(matrice):
    liste = []
    ...

# --------- PYODIDE:corr --------- #

def matrice_vers_liste(matrice):
    liste = []
    for i in range(len(matrice)):
        liste.append([])
        for j in range(len(matrice[i])):
            if matrice[i][j] == 1:
                liste[i].append(j)
    return liste

# --------- PYODIDE:tests --------- #

m = [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
assert matrice_vers_liste(m) == [[1, 2], [0, 2, 3], [0, 1, 3], [1, 2]]
m = [[0, 1], [1, 0]]
assert matrice_vers_liste(m) == [[1], [0]]
m = [[0, 0], [1, 0]]
assert matrice_vers_liste(m) == [[], [0]]
m = [[0, 0], [0, 0]]
assert matrice_vers_liste(m) == [[], []]
m = [[0]]
assert matrice_vers_liste(m) == [[]]
m = [[1]]
assert matrice_vers_liste(m) == [[0]]

# --------- PYODIDE:secrets --------- #


# tests secrets
m = [[0] * 10**3 for i in range(10**3)]
lst = [[] for i in range(10**3)]
assert matrice_vers_liste(m) == lst, "Erreur avec m = [[0] * 10**3 for i in range(10**3)]"
m = [[1] * 10**3 for i in range(10**3)]
lst = [[k for k in range(10**3)] for i in range(10**3)]
assert matrice_vers_liste(m) == lst, "Erreur avec m = [[1] * 10**3 for i in range(10**3)]"
m = [[[0, 1][i%2]] * 10**3 for i in range(10**3)]
lst = [[k for k in range(10**3)] if i%2 else [] for i in range(10**3)]
assert matrice_vers_liste(m) == lst, "Erreur avec m = [[[0, 1][i%2]] * 10**3 for i in range(10**3)]"