# --- hdr, fonction --- #
def g(x):
    pass


# --- exo, fonction --- #
def f(x):
    return 5 * x**2 - 9 * x + 3


# compléter ci-dessous avec la définition de la fonction g
...


# --- corr, fonction --- #
def g(x):
    return -3 * x**2 + 7 * x + 5


# --- tests, fonction --- #
# la fonction f n'a pas été modifiée
assert f(0) == 3
assert f(2) == 5
# définition correcte de la fonction g
assert g(0) == 5
assert g(2) == 7


# --- secrets, fonction --- #
def _g(x):
    return -3 * x**2 + 7 * x + 5


for x in range(-10, 11):
    attendu = _g(x)
    assert g(x) == attendu, f"Erreur sur la valeur obtenue pour g({x})"


# --- vide, valeurs_ --- #
def f(x):
    return x**2


def valeurs(fonction, x_min, x_max): 
    ...


# --- exo, valeurs_ --- #
def f(x):
    return x**2


def valeurs(fonction, x_min, x_max):
    tableau = []
    for x in range(..., ...):
        tableau.append(...)
    return ...


# --- corr, valeurs_ --- #
def valeurs(fonction, x_min, x_max):
    tableau = []
    for x in range(x_min, x_max + 1):
        tableau.append(fonction(x))
    return tableau


# --- tests, valeurs_ --- #
assert valeurs(f, 0, 1) == [0, 1]
assert valeurs(f, 0, 3) == [0, 1, 4, 9]


def g(x):
    return -2 * x**2 + 3 * x + 1


assert valeurs(g, -1, 1) == [-4, 1, 2]
assert valeurs(g, -1, 3) == [-4, 1, 2, -1, -8]

# --- secrets, valeurs_ --- #
a = -3
b = 4
c = -1


def h(x):
    return a * x**2 + b * x + c


x_min = -4
x_max = 5
attendu = [h(x) for x in range(x_min, x_max + 1)]
assert valeurs(h, x_min, x_max) == attendu, f"Erreur en prenant x ↦ {a}x**2+{b}x{c}"

a = -10
b = -3
c = 9


def h(x):
    return a * x**2 + b * x + c


x_min = -7
x_max = 12
attendu = [h(x) for x in range(x_min, x_max + 1)]
assert valeurs(h, x_min, x_max) == attendu, f"Erreur en prenant x ↦ {a}x**2{b}x+{c}"


# --- exo, sommet --- #
def sommet(a, b, c): 
    ...


# --- corr, sommet --- #
def sommet(a, b, c):
    x = -b / (2 * a)
    y = a * x**2 + b * x + c
    return (x, y)


# --- tests, sommet --- #
assert sommet(1, 0, 0) == (0.0, 0.0)
assert sommet(1, -8, 3) == (4.0, -13.0)

# --- secrets, sommet --- #
a = 3
b = -2
c = 4
attendu = -b / (2 * a), a * (-b / (2 * a)) ** 2 + b * (-b / (2 * a)) + c
assert sommet(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = 2
b = 0
c = 5
attendu = -b / (2 * a), a * (-b / (2 * a)) ** 2 + b * (-b / (2 * a)) + c
assert sommet(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = -2
b = 6
c = 0
attendu = -b / (2 * a), a * (-b / (2 * a)) ** 2 + b * (-b / (2 * a)) + c
assert sommet(a, b, c) == attendu, f"Erreur avec {a, b, c = }"


# --- exo, variations --- #
def variations(a, b, c):
    ...


# --- corr, variations --- #
def variations(a, b, c):
    if a < 0:
        return "^"
    else:
        return "v"


# --- tests, variations --- #
assert variations(1, 0, 0) == "v"
assert variations(-1, -8, 3) == "^"

# --- secrets, variations --- #
a = 3
b = -2
c = 4
attendu = "v"
assert variations(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = 2
b = 0
c = 5
attendu = "v"
assert variations(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = -2
b = 6
c = 0
attendu = "^"
assert variations(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
