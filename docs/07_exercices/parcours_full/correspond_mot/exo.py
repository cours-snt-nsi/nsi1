

# --------- PYODIDE:code --------- #

def correspond(mot_complet, mot_a_trous):
    ...

# --------- PYODIDE:corr --------- #

def correspond(mot_complet, mot_a_trous):
    if len(mot_complet) != len(mot_a_trous):
        return False
    for i in range(len(mot_complet)):
        if mot_a_trous[i] != "." and mot_a_trous[i] != mot_complet[i]:
            return False
    return True

# --------- PYODIDE:tests --------- #

assert correspond("INFORMATIQUE", "INFO.MA.IQUE") is True
assert correspond("AUTOMATIQUE", "INFO.MA.IQUE") is False
assert correspond("INFO", "INFO.MA.IQUE") is False
assert correspond("INFORMATIQUES", "INFO.MA.IQUE") is False

# --------- PYODIDE:secrets --------- #


# autres tests

assert correspond("", "") is True
assert correspond("A", "A") is True
assert correspond("A", ".") is True
assert correspond("A", "B") is False
assert correspond("TIQUE", "TIQUE") is True
assert correspond("TIQUE", ".IQUE") is True
assert correspond("TIQUE", "T.QUE") is True
assert correspond("TIQUE", "TI.UE") is True
assert correspond("TIQUE", "TIQ.E") is True
assert correspond("TIQUE", "TIQU.") is True
assert correspond("TIQUE", ".IQU.") is True
assert correspond("TIQUE", ".....") is True
assert correspond("TIQUE", "TIQUES") is False
assert correspond("TIQUE", "ATIQUE") is False
assert correspond("TIQUE", ".TIQUE") is False
assert correspond("TIQUE", "TIQUE.") is False
assert correspond("TIQUE", "TIQUF") is False
assert correspond("TIQUE", "PIQUE") is False

assert correspond("AB", "..") is True
assert correspond("BCA", ".B.") is False
assert correspond("A", "..") is False
assert correspond("A" * 101, "A" * 100 + ".") is True