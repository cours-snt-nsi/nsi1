

# --------- PYODIDE:code --------- #

ouvrant = "([{<"
fermant = ")]}>"
ouverture = {  # dictionnaire qui donne l'ouvrant en fonction du fermant
    ")": "(",
    "]": "[",
    "}": "{",
    ">": "<",
}


def est_bien_parenthesee(expression):
    ...

# --------- PYODIDE:corr --------- #

ouvrant = "([{<"
fermant = ")]}>"
ouverture = {f: o for o, f in zip(ouvrant, fermant)}


def est_bien_parenthesee(expression):
    pile = []
    for c in expression:
        if c in ouvrant:
            pile.append(c)
        elif c in fermant:
            if pile == [] or pile.pop() != ouverture[c]:
                return False
    return pile == []

# --------- PYODIDE:tests --------- #

assert est_bien_parenthesee("(2 + 4)*7") is True
assert est_bien_parenthesee("tableau[f(i) - g(i)]") is True
assert (
    est_bien_parenthesee(
        "int main(){int liste[2] = {4, 2}; return (10*liste[0] + liste[1]);}"
    )
    is True
)

assert est_bien_parenthesee("(une parenthèse laissée ouverte") is False
assert est_bien_parenthesee("{<(}>)") is False
assert est_bien_parenthesee("c'est trop tard ;-)") is False

# --------- PYODIDE:secrets --------- #


# autres tests


assert est_bien_parenthesee("a(b)c") is True
assert est_bien_parenthesee("a[]b") is True
assert est_bien_parenthesee("{ }") is True
assert est_bien_parenthesee("<a>") is True
assert est_bien_parenthesee("()[]{}<>") is True
assert est_bien_parenthesee("([{<>}])") is True
assert est_bien_parenthesee("<{[([{<>}])]}>") is True
assert (
    est_bien_parenthesee(
        "(" * 100
        + "[" * 100
        + "{" * 100
        + "<" * 100
        + ">" * 100
        + "}" * 100
        + "]" * 100
        + ")" * 100
    )
    is True
)

assert est_bien_parenthesee("a(bc") is False
assert est_bien_parenthesee("a]b") is False
assert est_bien_parenthesee("} {") is False
assert est_bien_parenthesee("><a><") is False
assert est_bien_parenthesee("([)]{<}>") is False
assert est_bien_parenthesee("([{<}>)]") is False
assert est_bien_parenthesee("{<([[<{>}])]}>") is False
assert (
    est_bien_parenthesee(
        "(" * 100
        + "[" * 100
        + "{" * 100
        + "<" * 100
        + "<(>)"
        + ">" * 100
        + "}" * 100
        + "]" * 100
        + ")" * 100
    )
    is False
)