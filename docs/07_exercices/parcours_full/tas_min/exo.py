

# --------- PYODIDE:code --------- #

def est_tas_min(tableau):
    ...

# --------- PYODIDE:corr --------- #

def est_tas_min(tableau):
    n = len(tableau) - 1
    for i in range(2, n + 1):
        if tableau[i // 2] > tableau[i]:
            return False
    return True

# --------- PYODIDE:tests --------- #

## exemples
assert est_tas_min([None, 10, 42, 23, 55, 67]) is True

assert (
    est_tas_min(
        [
            None,
            "brrr",
            "chut",
            "ouille",
            "dring",
            "tada",
            "vroum",
            "wahou",
            "paf",
            "hehe",
        ]
    )
    is True
)

assert est_tas_min([None]) is True

assert est_tas_min([None, 10, 10]) is True

## contre-exemples
assert est_tas_min([None, 10, 2]) is False

assert est_tas_min([None, "ba", "ab"]) is False

assert est_tas_min([None, 10, 42, 23, 30]) is False

assert est_tas_min([None, 10, 42, 23, 55, 40]) is False

# --------- PYODIDE:secrets --------- #


# autres tests

## exemples
assert est_tas_min([None, 110, 142, 123, 155, 167]) is True

assert (
    est_tas_min(
        [
            None,
            "zbrrr",
            "zchut",
            "zouille",
            "zdring",
            "ztada",
            "zvroum",
            "zwahou",
            "zpaf",
            "zhehe",
        ]
    )
    is True
)

assert est_tas_min([None, 100.0, 100.0]) is True


assert est_tas_min([None, 110, 142, 123, 155, 167, 125, 123, 156, 157]) is True
assert est_tas_min([None, 110, 142, 123, 155, 167, 125, 123, 156, 157, 168]) is True

## contre-exemples
assert est_tas_min([None, 110, 12]) is False

assert est_tas_min([None, "zba", "zab"]) is False

assert est_tas_min([None, 110, 142, 123, 130]) is False

assert est_tas_min([None, 110, 142, 123, 155, 140]) is False

## lourds
lourd = [None]
lourd.extend(range(10**5))
assert est_tas_min(lourd) is True

lourd = [None]
lourd.extend(range(10**5))
lourd[42] = 0
assert est_tas_min(lourd) is False