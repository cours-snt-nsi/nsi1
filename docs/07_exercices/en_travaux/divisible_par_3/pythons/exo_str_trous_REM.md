Il est possible d'optimiser cette approche en observant qu'il est inutile de tenir compte des chiffres `#!py 0`, `#!py 3`, `#!py 6` et `#!py 9` lors des additions (étant divisibles par `#!py 3`, ils peuvent être ignorés.).

La fonction devient alors :

```python
def divisible_par_3(n):
    while n > 10:
        chaine = str(n)
        somme = 0
        for chiffre in chaine:
            if chiffre not in "0369":
                somme = somme + int(chiffre)
        n = int(somme)
    return n in (0, 3, 6, 9)
```