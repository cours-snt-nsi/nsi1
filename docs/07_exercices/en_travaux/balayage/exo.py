# --- HDR, f_ --- #
racine_de_3 = 3**0.5


def largeur_valide(a, b, h):
    largeur = abs(b - a)
    return abs(largeur - h) < 1e-6


# --- vide, f_ --- #
def f(x):
    return x * x - 3


def balayage(f, a, h):
    ...


# --- exo, f_ --- #
""" # skip
def f(x):
    return x*x - 3

def balayage(f, a, h):
    x = ...
    while f(...) * f(...) ... 0:
        x = ...
    return (..., ...)
"""  # skip


# --- corr, f_ --- #
def f(x):
    return x * x - 3


def balayage(f, a, h):
    x = a
    while f(a) * f(x + h) >= 0:
        x = x + h
    return (x, x + h)


# --- tests, f_ --- #
x, x_plus_h = balayage(f, 0, 1)
# la solution est-elle comprise dans l'intervalle ?
assert x <= racine_de_3 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
# l'encadrement est-il bien de largeur 1 ?
assert largeur_valide(x, x_plus_h, 1), "L'encadrement n'a pas la largeur attendue"

x, x_plus_h = balayage(f, 1, 0.1)
assert x <= racine_de_3 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
assert largeur_valide(x, x_plus_h, 0.1), "L'encadrement n'a pas la largeur attendue"

x, x_plus_h = balayage(f, 1.7, 0.01)
assert x <= racine_de_3 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
assert largeur_valide(x, x_plus_h, 0.01), "L'encadrement n'a pas la largeur attendue"

# --- secrets, f_ --- #
a, h = 1.73, 0.001
x, x_plus_h = balayage(f, a, h)
assert x <= racine_de_3 < x_plus_h
assert largeur_valide(
    x, x_plus_h, 0.001
), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"

for n in range(4, 6):
    h = 10**-n
    a = 1
    x, x_plus_h = balayage(f, a, h)
    assert (
        x <= racine_de_3 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = }"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"

racine_de_5 = 5**0.5
f = lambda x: x * x - 5
for n in range(4, 6):
    a = 1
    h = 10**-n
    x, x_plus_h = balayage(f, a, h)
    assert (
        x <= racine_de_5 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = } et une autre fontion f"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"
    a = 2.23
    x, x_plus_h = balayage(f, a, h)
    assert (
        x <= racine_de_5 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = } et une autre fontion f"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"

# --- HDR , racine_2_ --- #
racine_de_2 = 2**0.5


def largeur_valide(a, b, h):
    largeur = abs(b - a)
    return abs(largeur - h) < 1e-6


# --- vide , racine_2_ --- #
def balayage(a, h):
    ...


# --- exo, racine_2_ --- #
""" # skip
def balayage(a, h):
    x = ...
    while ...:
        x = ...
    return (..., ...)


"""  # skip


# --- corr, racine_2_ --- #
def balayage(a, h):
    x = a
    while (x + h) ** 2 <= 2:
        x = x + h
    return (x, x + h)


# --- tests, racine_2_ --- #
x, x_plus_h = balayage(1, 0.1)
# la solution est-elle comprise dans l'intervalle ?
assert x <= racine_de_2 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
# l'encadrement est-il bien de largeur 0.1 ?
assert largeur_valide(x, x_plus_h, 0.1), "L'encadrement n'a pas la largeur attendue"

x, x_plus_h = balayage(1.4, 0.01)
assert x <= racine_de_2 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
assert largeur_valide(x, x_plus_h, 0.01), "L'encadrement n'a pas la largeur attendue"

# --- secrets, racine_2_ --- #
a, h = 1.41, 0.001
x, x_plus_h = balayage(a, h)
assert (
    x <= racine_de_2 < x_plus_h
), f"La racine carré de 2 n'est pas dans l'encadrement avec {(a, h) = }"
assert largeur_valide(x, x_plus_h, 0.001), "L'encadrement n'a pas la largeur attendue"

for n in range(4, 6):
    a = 1
    h = 10**-n
    x, x_plus_h = balayage(a, h)
    assert (
        x <= racine_de_2 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = }"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"
    a = 1.3
    x, x_plus_h = balayage(a, h)
    assert (
        x <= racine_de_2 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = }"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"
