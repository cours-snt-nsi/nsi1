---
author: Steeve PYTEL
title: 🌐 Web interactif

hide:
    - toc
---

# Chapitre 2 : Web interactif

![Picture](../../../assets/web1.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Picture](../../../assets/Roberta.jpg){ style="display: block; margin: 0 auto" }

    !!! tip "Roberta Williams"
        [Roberta Williams](https://fr.wikipedia.org/wiki/Roberta_Williams){:target="_blank"} (née le 16 février 1953) est connue pour être à l'origine du jeu vidéo Mystery House, le premier jeu d'aventure graphique, dont le succès fut tel qu'avec son mari Ken Williams, elle créa On-Line Systems en 1980 qui devint en 1982, Sierra On-Line. On lui doit de nombreux autres classiques du jeu d'aventure. La créatrice de jeu-vidéo américaine a mis au point Mystery House en 1980, avec son mari Ken Williams. Autrefois en mode texte uniquement, le monde du jeu-vidéo est alors propulsé dans une nouvelle dimension.

Le langage JavaScript est un langage de programmation interprété permettant de créer des sites Web *interactifs*, c'est-à-dire des sites Web qui puissent afficher du contenu en fonction d'événements réalisés côté client (clic de souris, survol d'une zone à l'aide de la souris, etc.).

Le but de ce chapitre est double :

- présenter et manipuler, à travers deux TP à faire en autonomie, quelques éléments clés du langage JavaScript ;
- comprendre les notions générales liées à la programmation événementielle.

On propose le déroulé suivant :

1. un premier TP (TP1 introduction) présentant quelques éléments clés du langage JavaScript avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront le document ressource *Introduction au JavaScript* disponible en téléchargement ;
2. un deuxième TP (TP2 manipulation) pour manipuler et approfondir les notions vues au TP précédent avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront les documents ressources *Introduction au HTML5*, *Introduction au CSS3* et *Introduction au JavaScript* disponibles en téléchargement ;
3. un point de cours bilan reprenant l'essentiel de ce qui a été vu dans les deux TP précédents ;
4. un troisième TP (TP3 projets) permettant de mettre en œuvre les notions vues précédemment en JavaScript à travers la réalisation d'un site Web interactif.

??? note "TP1 (introduction)"
    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Web_interactif_TP1.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

??? note "TP2 (manipulation)"
    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Web_interactif_TP2.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

??? note "Cours"
    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Web_interactif_cours.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

??? note "TP3 (projets)"
    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Web_interactif_TP3.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

### Téléchargement 

[Téléchargement des ressources](../../../documents/interactif.zip){:download="interactif.zip"}.

