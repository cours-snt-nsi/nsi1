---
author: Steeve PYTEL
title: 📎 Site mkdocs
---

# TP NSI : Création d'un site web avec MkDocs, GitLab et MkDocs Material

## Objectifs du TP
1. Comprendre les bases de la gestion de versions avec Git.
2. Apprendre à utiliser GitLab pour héberger un projet.
3. Créer et configurer un site web statique avec MkDocs.
4. Utiliser le thème MkDocs Material pour personnaliser le site.
5. Déployer le site sur GitLab Pages.

## Matériel et Pré-requis
- Un ordinateur avec un accès internet.
- Un compte GitLab.
- Python et pip installés sur l'ordinateur.
- Connaissances de base en HTML et Markdown.

## Étape 1 : Installation des outils
**Objectif : Installer les outils nécessaires pour le TP.**

1. **Installer MkDocs et MkDocs Material :**
    ```bash
    pip install mkdocs mkdocs-material
    ```

2. **Vérifier l'installation :**
    ```bash
    mkdocs --version
    ```

## Étape 2 : Initialisation du projet MkDocs
**Objectif : Créer la structure de base d'un site MkDocs.**

1. **Créer un nouveau répertoire pour le projet :**
    ```bash
    mkdir mon-site-mkdocs
    cd mon-site-mkdocs
    ```

2. **Initialiser un nouveau projet MkDocs :**
    ```bash
    mkdocs new .
    ```

3. **Tester le site localement :**
    ```bash
    mkdocs serve
    ```

4. **Accéder au site via le navigateur à l'adresse :**
    ```
    http://127.0.0.1:8000/
    ```

## Étape 3 : Configuration de Git et GitLab
**Objectif : Initialiser un dépôt Git et le pousser sur GitLab.**

1. **Initialiser un dépôt Git localement :**
    ```bash
    git init
    git add .
    git commit -m "Initial commit"
    ```

2. **Créer un nouveau projet sur GitLab :**
    - Aller sur [GitLab](https://gitlab.com/)
    - Créer un nouveau projet (Public ou Privé selon les préférences)

3. **Ajouter le dépôt GitLab comme remote et pousser les modifications :**
    ```bash
    git remote add origin https://gitlab.com/<votre-utilisateur>/mon-site-mkdocs.git
    git branch -M main
    git push -u origin main
    ```

## Étape 4 : Configuration de MkDocs Material
**Objectif : Personnaliser le site avec le thème MkDocs Material.**

1. **Modifier le fichier `mkdocs.yml` pour utiliser le thème Material :**
    ```yaml
    site_name: Mon Site MkDocs
    theme:
      name: material
    ```

2. **Ajouter des configurations supplémentaires (facultatif) :**
    ```yaml
    theme:
      name: material
      language: fr
      palette:
        primary: 'indigo'
        accent: 'indigo'
      font:
        text: 'Roboto'
        code: 'Roboto Mono'
    ```

3. **Ajouter du contenu en modifiant les fichiers Markdown dans le dossier `docs`.**

## Étape 5 : Déploiement sur GitLab Pages
**Objectif : Configurer GitLab CI/CD pour déployer le site sur GitLab Pages.**

1. **Créer un fichier `.gitlab-ci.yml` à la racine du projet :**
    ```yaml
    image: python:3.8

    pages:
      script:
        - pip install mkdocs mkdocs-material
        - mkdocs build
      artifacts:
        paths:
          - public
      only:
        - main
    ```

2. **Commiter et pousser ce fichier :**
    ```bash
    git add .gitlab-ci.yml
    git commit -m "Ajout de la configuration GitLab CI/CD"
    git push
    ```

3. **Vérifier le déploiement :**
    - Aller dans la section "CI/CD > Pipelines" sur GitLab pour voir l'état du déploiement.
    - Une fois le déploiement terminé, accéder au site via l'URL GitLab Pages : `https://<votre-utilisateur>.gitlab.io/mon-site-mkdocs`.

## Étape 6 : Extensions et Personnalisation
**Objectif : Ajouter des fonctionnalités avancées et personnaliser davantage le site.**

1. **Ajouter des extensions (par exemple, pour les tableaux, les alertes, etc.) :**
    ```yaml
    markdown_extensions:
      - admonition
      - codehilite
      - tables
    ```

2. **Personnaliser la navigation en modifiant `mkdocs.yml` :**
    ```yaml
    nav:
      - Home: index.md
      - About: about.md
      - Contact: contact.md
    ```

## Conclusion
**Objectif : Finaliser le projet et réfléchir aux points d'amélioration.**

1. **Vérifier l'ensemble du site pour s'assurer qu'il n'y a pas d'erreurs.**
2. **Encourager les élèves à personnaliser leur site selon leurs goûts et à explorer les fonctionnalités avancées de MkDocs et MkDocs Material.**
