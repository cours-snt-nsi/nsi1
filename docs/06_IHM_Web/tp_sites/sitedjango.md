---
author: Steeve PYTEL
title: 📎 Site django
---

# TP NSI : Création d'un site web avec Django et GitLab

## Objectifs du TP
1. Comprendre les bases de la gestion de versions avec Git.
2. Apprendre à utiliser GitLab pour héberger un projet.
3. Créer et configurer un site web avec Django.
4. Déployer le site sur un service de cloud comme Heroku.

## Matériel et Pré-requis
- Un ordinateur avec un accès internet.
- Un compte GitLab.
- Python et pip installés sur l'ordinateur.

## Étape 1 : Installation des outils
**Objectif : Installer les outils nécessaires pour le TP.**

1. **Installer Django :**
    ```bash
    pip install django
    ```

2. **Vérifier l'installation :**
    ```bash
    django-admin --version
    ```

## Étape 2 : Création du projet Django
**Objectif : Créer la structure de base d'un projet Django.**

1. **Créer un nouveau répertoire pour le projet :**
    ```bash
    mkdir mon-projet-django
    cd mon-projet-django
    ```

2. **Créer un nouveau projet Django :**
    ```bash
    django-admin startproject monsite
    cd monsite
    ```

3. **Créer une nouvelle application Django :**
    ```bash
    python manage.py startapp main
    ```

4. **Tester le site localement :**
    ```bash
    python manage.py runserver
    ```

5. **Accéder au site via le navigateur à l'adresse :**
    ```
    http://127.0.0.1:8000/
    ```

## Étape 3 : Configuration de Git et GitLab
**Objectif : Initialiser un dépôt Git et le pousser sur GitLab.**

1. **Initialiser un dépôt Git localement :**
    ```bash
    git init
    git add .
    git commit -m "Initial commit"
    ```

2. **Créer un nouveau projet sur GitLab :**
    - Aller sur [GitLab](https://gitlab.com/)
    - Créer un nouveau projet (Public ou Privé selon les préférences)

3. **Ajouter le dépôt GitLab comme remote et pousser les modifications :**
    ```bash
    git remote add origin https://gitlab.com/<votre-utilisateur>/mon-projet-django.git
    git branch -M main
    git push -u origin main
    ```

## Étape 4 : Déploiement sur Heroku
**Objectif : Configurer et déployer le site sur Heroku.**

1. **Installer l'outil de ligne de commande Heroku :**
    ```bash
    curl https://cli-assets.heroku.com/install.sh | sh
    ```

2. **Créer un fichier `requirements.txt` :**
    ```bash
    pip freeze > requirements.txt
    ```

3. **Créer un fichier `Procfile` pour spécifier les commandes à exécuter :**
    ```
    web: gunicorn monsite.wsgi
    ```

4. **Commiter et pousser ces fichiers :**
    ```bash
    git add requirements.txt Procfile
    git commit -m "Ajout des fichiers de déploiement"
    git push
    ```

5. **Créer une application sur Heroku et déployer :**
    ```bash
    heroku create mon-projet-django
    git push heroku main
    ```

## Conclusion
**Objectif : Finaliser le projet et réfléchir aux points d'amélioration.**

1. **Vérifier l'ensemble du site pour s'assurer qu'il n'y a pas d'erreurs.**
2. **Encourager les élèves à personnaliser leur site selon leurs goûts et à explorer les fonctionnalités avancées de Django.**
