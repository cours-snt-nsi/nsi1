---
author: Steeve PYTEL
title: 📎 Site Flask
---

# TP NSI : Création d'un site web avec Flask et GitLab

## Objectifs du TP
1. Comprendre les bases de la gestion de versions avec Git.
2. Apprendre à utiliser GitLab pour héberger un projet.
3. Créer et configurer un site web avec Flask.
4. Déployer le site sur un service de cloud comme Heroku.

## Matériel et Pré-requis
- Un ordinateur avec un accès internet.
- Un compte GitLab.
- Python et pip installés sur l'ordinateur.

## Étape 1 : Installation des outils
**Objectif : Installer les outils nécessaires pour le TP.**

1. **Installer Flask :**
    ```bash
    pip install flask
    ```

2. **Vérifier l'installation :**
    ```bash
    python -c "import flask; print(flask.__version__)"
    ```

## Étape 2 : Création de l'application Flask
**Objectif : Créer la structure de base d'une application Flask.**

1. **Créer un nouveau répertoire pour le projet :**
    ```bash
    mkdir mon-app-flask
    cd mon-app-flask
    ```

2. **Créer un fichier Python pour l'application :**
    ```python
    # fichier app.py
    from flask import Flask

    app = Flask(__name__)

    @app.route('/')
    def home():
        return "Bienvenue dans mon application Flask"

    if __name__ == '__main__':
        app.run(debug=True)
    ```

3. **Tester l'application localement :**
    ```bash
    python app.py
    ```

4. **Accéder à l'application via le navigateur à l'adresse :**
    ```
    http://127.0.0.1:5000/
    ```

## Étape 3 : Configuration de Git et GitLab
**Objectif : Initialiser un dépôt Git et le pousser sur GitLab.**

1. **Initialiser un dépôt Git localement :**
    ```bash
    git init
    git add .
    git commit -m "Initial commit"
    ```

2. **Créer un nouveau projet sur GitLab :**
    - Aller sur [GitLab](https://gitlab.com/)
    - Créer un nouveau projet (Public ou Privé selon les préférences)

3. **Ajouter le dépôt GitLab comme remote et pousser les modifications :**
    ```bash
    git remote add origin https://gitlab.com/<votre-utilisateur>/mon-app-flask.git
    git branch -M main
    git push -u origin main
    ```

## Étape 4 : Déploiement sur Heroku
**Objectif : Configurer et déployer l'application sur Heroku.**

1. **Installer l'outil de ligne de commande Heroku :**
    ```bash
    curl https://cli-assets.heroku.com/install.sh | sh
    ```

2. **Créer un fichier `requirements.txt` :**
    ```bash
    pip freeze > requirements.txt
    ```

3. **Créer un fichier `Procfile` pour spécifier les commandes à exécuter :**
    ```
    web: gunicorn app:app
    ```

4. **Commiter et pousser ces fichiers :**
    ```bash
    git add requirements.txt Procfile
    git commit -m "Ajout des fichiers de déploiement"
    git push
    ```

5. **Créer une application sur Heroku et déployer :**
    ```bash
    heroku create mon-app-flask
    git push heroku main
    ```

## Conclusion
**Objectif : Finaliser le projet et réfléchir aux points d'amélioration.**

1. **Vérifier l'ensemble de l'application pour s'assurer qu'il n'y a pas d'erreurs.**
2. **Encourager les élèves à personnaliser leur application selon leurs goûts et à explorer les fonctionnalités avancées de Flask.**
