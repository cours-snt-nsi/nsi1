---
author: Steeve PYTEL
title: 📎 TP
hide:
    -toc
---

# TP NSI : Création d'un site web avec Python et GitLab

#### Objectifs du TP
1. Apprendre à utiliser différentes technologies web : Django, Flask, MkDocs, ou Streamlit.
2. Collaborer en groupe pour créer un site web complet et fonctionnel.
3. Réinvestir les connaissances acquises en programmation, gestion de projet et rédaction de contenu.
4. Mettre en pratique la gestion de version avec Git et GitLab.

#### Consignes Générales
- Les élèves se regroupent par groupes de 2 à 4 personnes.
- Chaque groupe choisit une technologie parmi Django, Flask, MkDocs, ou Streamlit pour réaliser leur site.
- Chaque groupe choisit un thème pour leur site parmi une liste donnée.
- Le site doit comporter au minimum 5 pages, incluant des illustrations et du contenu sourcé.
- Chaque membre du groupe aura un rôle défini : rédacteur, graphiste, concepteur, chef de projet.

#### Rôles au sein des groupes
- **Rédacteur** : Responsable de la rédaction des textes et de la recherche des sources.
- **Graphiste** : Responsable des illustrations, de la mise en page et du design.
- **Concepteur** : Responsable de la structure technique du site et de l'intégration du contenu.
- **Chef de projet** : Responsable de la coordination du groupe, de la gestion des tâches et des échéances.

#### Thèmes proposés pour le site

1. **Blog sur les Technologies de l'Information et de la Communication (TIC)** :
    - Articles sur les dernières tendances en TIC.
    - Tutoriels sur des logiciels ou des langages de programmation.
    - Analyses des impacts des TIC sur la société.

2. **Site d'actualité sur les avancées scientifiques** :
    - Articles sur les dernières découvertes en biologie, physique, chimie, etc.
    - Interviews de scientifiques et chercheurs.
    - Rubriques sur les applications pratiques des nouvelles découvertes.

3. **Portail éducatif pour réviser les matières du programme** :
    - Cours en ligne et résumés de chapitres.
    - Exercices interactifs et quiz pour tester les connaissances.
    - Vidéos explicatives et tutoriels.

4. **Site de sensibilisation à l'écologie et au développement durable** :
    - Articles sur les enjeux environnementaux.
    - Conseils pratiques pour adopter un mode de vie écologique.
    - Projets et initiatives locales ou globales en faveur de l'environnement.

5. **Plateforme de partage de projets de programmation** :
    - Présentation de projets réalisés par les élèves (jeux, applications, sites web).
    - Tutoriels pour reproduire les projets.
    - Forum pour échanger des idées et des conseils.

6. **Journal scolaire en ligne** :
    - Articles sur la vie scolaire et les événements à venir.
    - Interviews des enseignants et des élèves.
    - Sections dédiées aux clubs et associations de l'école.

7. **Site de vulgarisation scientifique** :
    - Explications simplifiées de concepts scientifiques complexes.
    - Expériences à réaliser à la maison ou en classe.
    - Portraits de grands scientifiques et leurs contributions.

8. **Portail culturel** :
    - Critiques de livres, films, séries, expositions, etc.
    - Articles sur l'histoire de l'art et des mouvements artistiques.
    - Présentation de jeunes artistes et de leurs œuvres.

9. **Site sur la vie d'une grande figure de l'informatique** :
    - Biographie détaillée d'une figure emblématique (par exemple, Alan Turing, Ada Lovelace, Grace Hopper, Tim Berners-Lee, etc.).
    - Présentation des principales contributions de cette personne à l'informatique.
    - Impact de leurs travaux sur la technologie et la société actuelles.

10. **Site sur les métiers et les formations** :
    - Description de différents métiers, parcours de formation, débouchés.
    - Interviews de professionnels.
    - Conseils pour l'orientation et la recherche de stage.


#### Étapes du projet
1. **Choix de la technologie et du thème** : Chaque groupe choisit une technologie (Django, Flask, MkDocs, Streamlit) et un thème parmi la liste fournie.
2. **Planification du site** :
    - Définir la structure du site et les pages à créer.
    - Attribuer les rôles et les tâches au sein du groupe.
3. **Création du site** :
    - Rédaction du contenu.
    - Création des illustrations et du design.
    - Développement et intégration technique.
4. **Révision et finalisation** :
    - Vérification des sources et de la qualité du contenu.
    - Tests du site pour s'assurer qu'il fonctionne correctement.
    - Déploiement du site sur GitLab.

#### Liste de 20 personnalités du monde de l'informatique 
1. **Ada Lovelace** : Mathématicienne et pionnière de la programmation informatique.
2. **Grace Hopper** : Informaticienne et contre-amiral de la Marine américaine, inventrice du premier compilateur.
3. **Katherine Johnson** : Mathématicienne de la NASA ayant contribué aux premiers vols spatiaux habités.
4. **Margaret Hamilton** : Ingénieure en logiciel pour le programme Apollo.
5. **Hedy Lamarr** : Inventrice ayant contribué au développement du GPS et du Wi-Fi.
6. **Radia Perlman** : Conceptrice du Spanning Tree Protocol (STP).
7. **Annie Easley** : Programmeuse et mathématicienne à la NASA.
8. **Barbara Liskov** : Informaticienne ayant contribué aux principes de la programmation orientée objet.
9. **Frances E. Allen** : Première femme à recevoir le prix Turing pour ses travaux en optimisation de compilateurs.
10. **Marissa Mayer** : Ancienne CEO de Yahoo et ingénieure chez Google.
11. **Susan Wojcicki** : CEO de YouTube.
12. **Sheryl Sandberg** : COO de Facebook.
13. **Rana el Kaliouby** : Scientifique en informatique émotionnelle.
14. **Joan Clarke** : Cryptanalyste britannique ayant travaillé à Bletchley Park.
15. **Evelyn Boyd Granville** : Une des premières femmes afro-américaines à obtenir un doctorat en mathématiques.
16. **Jean Sammet** : Conceptrice du langage de programmation COBOL.
17. **Edith Clarke** : Première femme ingénieure électricienne professionnelle.
18. **Carol Shaw** : Une des premières conceptrices de jeux vidéo.
19. **Dorothy Vaughan** : Mathématicienne et informaticienne ayant travaillé pour la NASA.
20. **Karen Sparck Jones** : Informaticienne ayant contribué au développement du traitement du langage naturel.

#### Critères d'évaluation
- **Contenu** (30%) : Qualité et pertinence des informations, respect des sources.
- **Design** (20%) : Esthétique du site, qualité des illustrations.
- **Technique** (30%) : Fonctionnalité du site, qualité du code.
- **Organisation** (20%) : Répartition des rôles, respect des délais, collaboration au sein du groupe.

#### Livrables
- Le site web fonctionnel hébergé sur GitLab.
- Un rapport décrivant le rôle de chaque membre, les étapes de la création du site, et les sources utilisées.

---


Voici un exemple de plan détaillé pour le rapport à livrer à la fin du TP :

---

# Rapport de Projet : Création d'un Site Web avec [Technologie Choisie]

## 1. Introduction
### 1.1 Présentation du projet
- Objectif du projet
- Technologie choisie (Django, Flask, MkDocs, Streamlit)
- Thème du site

### 1.2 Membres de l'équipe et rôles
- Nom et prénom des membres
- Rôles attribués (rédacteur, graphiste, concepteur, chef de projet)

## 2. Planification du Projet
### 2.1 Choix du thème
- Description du thème choisi
- Justification du choix

### 2.2 Structure du site
- Plan du site (arborescence des pages)
- Description de chaque page

### 2.3 Répartition des tâches
- Tableau ou liste des tâches attribuées à chaque membre
- Calendrier de réalisation

## 3. Développement du Site
### 3.1 Contenu rédactionnel
- Processus de recherche des informations
- Description des sources utilisées (bibliographie)

### 3.2 Conception graphique
- Choix des illustrations et design
- Outils utilisés pour la création graphique

### 3.3 Développement technique
- Description de l'installation et configuration de la technologie choisie
- Étapes de développement du site
- Challenges techniques rencontrés et solutions apportées

## 4. Collaboration et Gestion de Projet
### 4.1 Communication au sein de l'équipe
- Moyens de communication utilisés (réunions, outils de gestion de projet, etc.)
- Fréquence et contenu des réunions

### 4.2 Gestion des versions avec Git et GitLab
- Processus de gestion de versions (commits, branches, merge requests)
- Problèmes rencontrés et résolution

## 5. Test et Déploiement
### 5.1 Tests réalisés
- Types de tests effectués (fonctionnels, unitaires, intégration)
- Bugs détectés et corrigés

### 5.2 Déploiement
- Description du processus de déploiement sur GitLab
- Difficultés rencontrées et solutions apportées

## 6. Résultats et Conclusion
### 6.1 Résultats obtenus
- Fonctionnalités du site
- Retour sur les objectifs initiaux et leur atteinte

### 6.2 Bilan personnel de chaque membre
- Apports personnels et compétences acquises
- Points forts et axes d'amélioration

### 6.3 Perspectives d'amélioration
- Améliorations possibles du site
- Idées pour des développements futurs

## 7. Annexes
- Code source des principales fonctionnalités
- Captures d'écran du site
- Autres documents pertinents

---

