---
author: Steeve PYTEL
title: 📎 Site streamlit
---

# TP NSI : Création d'une application web avec Streamlit et GitLab

## Objectifs du TP
1. Comprendre les bases de la gestion de versions avec Git.
2. Apprendre à utiliser GitLab pour héberger un projet.
3. Créer et configurer une application web avec Streamlit.
4. Déployer l'application sur Streamlit Cloud.

## Matériel et Pré-requis
- Un ordinateur avec un accès internet.
- Un compte GitLab.
- Python et pip installés sur l'ordinateur.
- Connaissances de base en Python.

## Étape 1 : Installation des outils
**Objectif : Installer les outils nécessaires pour le TP.**

1. **Installer Streamlit :**
    ```bash
    pip install streamlit
    ```

2. **Vérifier l'installation :**
    ```bash
    streamlit hello
    ```

## Étape 2 : Création de l'application Streamlit
**Objectif : Créer la structure de base d'une application Streamlit.**

1. **Créer un nouveau répertoire pour le projet :**
    ```bash
    mkdir mon-app-streamlit
    cd mon-app-streamlit
    ```

2. **Créer un fichier Python pour l'application :**
    ```python
    # fichier app.py
    import streamlit as st

    st.title("Bienvenue dans mon application Streamlit")
    st.write("Ceci est une application web simple créée avec Streamlit.")
    ```

3. **Tester l'application localement :**
    ```bash
    streamlit run app.py
    ```

4. **Accéder à l'application via le navigateur à l'adresse :**
    ```
    http://localhost:8501
    ```

## Étape 3 : Configuration de Git et GitLab
**Objectif : Initialiser un dépôt Git et le pousser sur GitLab.**

1. **Initialiser un dépôt Git localement :**
    ```bash
    git init
    git add .
    git commit -m "Initial commit"
    ```

2. **Créer un nouveau projet sur GitLab :**
    - Aller sur [GitLab](https://gitlab.com/)
    - Créer un nouveau projet (Public ou Privé selon les préférences)

3. **Ajouter le dépôt GitLab comme remote et pousser les modifications :**
    ```bash
    git remote add origin https://gitlab.com/<votre-utilisateur>/mon-app-streamlit.git
    git branch -M main
    git push -u origin main
    ```

## Étape 4 : Configuration de l'environnement de déploiement
**Objectif : Configurer les fichiers nécessaires pour déployer l'application sur Streamlit Cloud.**

1. **Créer un fichier `requirements.txt` pour lister les dépendances :**
    ```
    streamlit
    ```

2. **Commiter et pousser ce fichier :**
    ```bash
    git add requirements.txt
    git commit -m "Ajout des dépendances"
    git push
    ```

## Étape 5 : Déploiement sur Streamlit Cloud
**Objectif : Déployer l'application sur Streamlit Cloud.**

1. **Aller sur [Streamlit Cloud](https://share.streamlit.io/) et se connecter.**
2. **Cliquer sur "New app" et sélectionner le dépôt GitLab.**
3. **Configurer les paramètres de déploiement (branche principale, fichier principal `app.py`).**
4. **Déployer l'application.**

## Étape 6 : Extensions et Personnalisation
**Objectif : Ajouter des fonctionnalités avancées et personnaliser davantage l'application.**

1. **Ajouter des widgets Streamlit (par exemple, des sélecteurs, des graphiques, etc.) :**
    ```python
    # fichier app.py
    import streamlit as st

    st.title("Bienvenue dans mon application Streamlit")
    st.write("Ceci est une application web simple créée avec Streamlit.")

    option = st.selectbox(
        'Choisissez une option:',
        ['Option 1', 'Option 2', 'Option 3']
    )

    st.write('Vous avez choisi:', option)
    ```

2. **Personnaliser l'apparence de l'application en modifiant les options de thème (facultatif).**

## Conclusion
**Objectif : Finaliser le projet et réfléchir aux points d'amélioration.**

1. **Vérifier l'ensemble de l'application pour s'assurer qu'il n'y a pas d'erreurs.**
2. **Encourager les élèves à personnaliser leur application selon leurs goûts et à explorer les fonctionnalités avancées de Streamlit.**
