---
author: Steeve PYTEL
title: 📎 Tuto Gitlab
hide:
    -toc
---

### Tuto GitLab : Commandes de Versionning Git

#### Introduction à GitLab
GitLab est une plateforme de gestion de dépôt Git qui permet de collaborer sur des projets de développement logiciel. Il offre des fonctionnalités pour la gestion de versions, l'intégration continue et le déploiement continu (CI/CD), ainsi que la gestion de projets et la collaboration.

### Prérequis
- Avoir Git installé sur votre ordinateur.
- Avoir un compte GitLab.
- Avoir un projet GitLab configuré (vous pouvez créer un nouveau projet depuis l'interface de GitLab).

### Étape 1 : Configuration initiale de Git

Avant de commencer à utiliser Git, configurez vos informations utilisateur :

```bash
git config --global user.name "Votre Nom"
git config --global user.email "votre.email@example.com"
```

### Étape 2 : Initialisation d'un dépôt Git local

Si vous n'avez pas encore de dépôt Git local, vous pouvez en créer un :

```bash
mkdir mon-projet
cd mon-projet
git init
```

### Étape 3 : Lier le dépôt local à un dépôt GitLab

Créez un nouveau projet sur GitLab et obtenez l'URL du dépôt (quelque chose comme `https://gitlab.com/username/mon-projet.git`). Ajoutez cette URL comme remote à votre dépôt local :

```bash
git remote add origin https://gitlab.com/username/mon-projet.git
```

### Étape 4 : Gestion de versions avec Git

#### 4.1 Ajouter des fichiers au dépôt

Pour suivre des fichiers avec Git, utilisez la commande `git add` :

```bash
git add nom_du_fichier
# ou pour ajouter tous les fichiers modifiés
git add .
```

#### 4.2 Valider (commit) des changements

Une fois que vous avez ajouté les fichiers, vous pouvez valider vos changements :

```bash
git commit -m "Message de commit"
```

#### 4.3 Pousser les changements vers GitLab

Pour envoyer vos commits locaux vers le dépôt distant sur GitLab :

```bash
git push origin main
```

Note : La branche par défaut sur GitLab est souvent `main`, mais elle peut être `master` selon la configuration.

#### 4.4 Récupérer les changements depuis GitLab

Pour mettre à jour votre dépôt local avec les dernières modifications du dépôt distant :

```bash
git pull origin main
```

#### 4.5 Gestion des branches

Les branches vous permettent de travailler sur différentes versions de votre projet en parallèle.

- **Créer une nouvelle branche** :

```bash
git checkout -b ma-nouvelle-branche
```

- **Basculer entre les branches** :

```bash
git checkout main
```

- **Fusionner une branche dans une autre** :

D'abord, assurez-vous d'être sur la branche dans laquelle vous voulez fusionner :

```bash
git checkout main
git merge ma-nouvelle-branche
```

- **Pousser une nouvelle branche vers GitLab** :

```bash
git push origin ma-nouvelle-branche
```

#### 4.6 Supprimer une branche

- **Supprimer une branche localement** :

```bash
git branch -d ma-nouvelle-branche
```

- **Supprimer une branche sur GitLab** :

```bash
git push origin --delete ma-nouvelle-branche
```

### Étape 5 : Utilisation des merge requests sur GitLab

Les merge requests permettent de demander la fusion des modifications d'une branche dans une autre, généralement pour permettre une revue de code.

1. **Créer une merge request** :
   - Allez sur votre projet sur GitLab.
   - Allez dans "Merge Requests".
   - Cliquez sur "New merge request".
   - Sélectionnez la branche source et la branche cible, puis cliquez sur "Compare branches and continue".
   - Ajoutez un titre et une description, puis cliquez sur "Create merge request".

2. **Revue et fusion d'une merge request** :
   - Les membres de l'équipe peuvent commenter et approuver la merge request.
   - Une fois approuvée, la merge request peut être fusionnée en cliquant sur "Merge".

### Étape 6 : Résolution des conflits

Parfois, des conflits surviennent lors de la fusion de branches. Voici comment les résoudre :

1. **Identifier les conflits** :

Lorsque vous essayez de fusionner des branches et qu'il y a des conflits, Git vous le dira. Les fichiers en conflit seront marqués.

2. **Résoudre les conflits** :

Ouvrez les fichiers en conflit et résolvez manuellement les différences entre les branches. Les sections conflictuelles sont marquées comme suit :

```plaintext
<<<<<<< HEAD
Votre code actuel
=======
Le code de la branche fusionnée
>>>>>>> nom-de-la-branche
```

Supprimez les marqueurs et faites les modifications nécessaires.

3. **Ajouter et valider les fichiers résolus** :

```bash
git add nom_du_fichier_conflit
git commit -m "Résolution des conflits"
```

Pour revenir à une version précédente dans Git, vous pouvez utiliser la commande `git checkout`. Voici comment procéder en fonction de ce que vous souhaitez faire :

### Revenir à une version précédente d'un fichier spécifique

Si vous souhaitez revenir à une version précédente d'un fichier spécifique dans votre dépôt :

```bash
git checkout <commit_hash> -- chemin/vers/le/fichier
```

Remplacez `<commit_hash>` par le hash du commit vers lequel vous souhaitez revenir. Le chemin vers le fichier doit être spécifié à partir de la racine du dépôt.

### Revenir à une version précédente de l'ensemble du projet

Si vous souhaitez revenir à une version précédente de l'ensemble du projet (c'est-à-dire tous les fichiers) :

1. Tout d'abord, trouvez le hash du commit vers lequel vous souhaitez revenir en utilisant `git log` pour voir l'historique des commits :

   ```bash
   git log
   ```

   Copiez le hash du commit auquel vous souhaitez revenir.

2. Utilisez `git checkout` avec le hash du commit pour revenir à cette version précédente :

   ```bash
   git checkout <commit_hash>
   ```

Cela vous placera dans un état de "detached HEAD" (tête détachée), ce qui signifie que vous êtes sur une branche sans nom et que tout nouveau commit ne sera pas sur une branche. Si vous souhaitez conserver ces changements, créez une nouvelle branche à partir de là :

```bash
git checkout -b nouvelle_branche
```

### Attention :
- Assurez-vous de comprendre que `git checkout` vous permet de naviguer entre différentes versions de votre projet, mais il peut également supprimer les modifications non enregistrées. Si vous avez des modifications en cours qui ne sont pas encore validées (commitées), elles peuvent être perdues en utilisant `git checkout` pour changer de version.
- Pour annuler des modifications locales non validées, utilisez `git reset --hard HEAD` pour revenir à la dernière version commitée sans modifications.

Utilisez ces commandes avec prudence, surtout lorsque vous modifiez l'historique de votre dépôt, car cela peut affecter la collaboration et la cohérence du code entre les membres de l'équipe.

### Conclusion

Avec ces commandes et pratiques de base, vous devriez être en mesure de gérer efficacement les versions de votre code en utilisant Git et GitLab. La gestion de versions est essentielle pour le développement collaboratif, vous permettant de suivre les changements, de travailler sur des fonctionnalités en parallèle et de maintenir un historique clair et organisé de votre projet.