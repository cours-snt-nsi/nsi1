---
author: Steeve PYTEL
title: 📎 IHM Web
---

# Interactions entre l’homme et la machine sur le Web


!!! abstract "Présentation"

    Lors de la navigation sur le Web, les internautes interagissent avec leur machine par le biais des pages Web.  
    L’Interface Homme-Machine (IHM) repose sur la gestion d’événements associés à des éléments graphiques munis de méthodes algorithmiques.  
    La compréhension du dialogue client-serveur déjà abordé en classe de seconde est consolidée, sur des exemples simples, en identifiant les requêtes du client, les calculs puis les réponses du serveur traitées par le client.  
    Il ne s’agit pas de décrire exhaustivement les différents éléments disponibles, ni de développer une expertise dans les langages qui permettent de mettre en œuvre le dialogue tels que PHP ou JavaScript. 

|Contenus|Capacités attendues|
|:--- | :--- |
|Modalités de l’interaction entre l’homme et la machine Événements|Identifier les différents composants graphiques permettant d’interagir avec une application Web. Identifier les événements que les fonctions associées aux différents composants graphiques sont capables de traiter|
|Interaction avec l’utilisateur dans une page Web|Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web.|
|Interaction clientserveur. Requêtes HTTP, réponses du serveur|Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre. Distinguer ce qui est mémorisé dans le client et retransmis au serveur. Reconnaître quand et pourquoi la transmission est chiffrée.|
|Formulaire d’une page Web|Analyser le fonctionnement d’un formulaire simple. Distinguer les transmissions de paramètres par les requêtes POST ou GET.|
