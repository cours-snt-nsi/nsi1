---
title: ⚡Accueil
hide:
    -toc
    -navigation
---
!!! success "Site NSI Lycee Jean Prevost"

    ![](./assets/LogoLaForge.svg){ width=50% }![](./assets/qrcode_atelier_forge.png){ width=20% align=right}

    Ce site est le document d'accompagnement de la SNT ET NSI (2024-2025).

!!! abstract "Bienvenue sur notre plateforme de cours en ligne !"

    ## [Numérique et Sciences Informatiques] 
    La NSI est une nouvelle matière passionnante qui vous initiera aux fondements de l'informatique et du numérique, en vous apprenant à concevoir et coder vos propres programmes. Que vous soyez déjà passionné ou simplement curieux, la NSI vous permettra d'acquérir des compétences essentielles pour comprendre et maîtriser les technologies d'aujourd'hui et de demain.

    Le programme de l'année de Première est [téléchargeable ici](/documents/BO.pdf){:download="BO.pdf"}.

