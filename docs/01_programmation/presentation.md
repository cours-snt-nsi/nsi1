---
author: Steeve PYTEL
title: 💻 Présentation
---

# Langages et programmation


!!! abstract "Présentation"

    Les langages de programmation Turing-complets sont caractérisés par un corpus de « constructions élémentaires ». Sans introduire cette terminologie, il s’agit de montrer qu’il existe de nombreux langages de programmation, différents par leur style (impératif, fonctionnel, objet, logique, événementiel, etc.), ainsi que des langages formalisés de description ou de requêtes  qui ne sont pas des langages de programmation.  
    L’importance de la spécification, de la documentation et des tests est à présenter, ainsi que l’intérêt de la modularisation qui permet la réutilisation de programmes et la mise à disposition de bibliothèques. Pour les programmes simples écrits par les élèves, on peut se contenter d’une spécification rapide mais précise. 

|Contenus|Capacités attendues|
|:--- | :--- |
|Constructions élémentaires|Mettre en évidence un corpus de constructions élémentaires.|
|Diversité et unité des langages de programmation|Repérer, dans un nouveau langage de programmation, les traits communs et les traits particuliers à ce langage.|
|Spécification|Prototyper une fonction. Décrire les préconditions sur les arguments. Décrire des postconditions sur les résultats.|
|Mise au point de programmes|Utiliser des jeux de tests.|
|Utilisation de bibliothèques|Utiliser la documentation d’une bibliothèque|

Les langages de programmation Turing-complets sont caractérisés par un corpus de _constructions élémentaires_. Sans introduire cette terminologie, il s’agit de montrer qu’il existe de nombreux langages de programmation, différents par leur style (impératif, fonctionnel, objet, logique, événementiel, etc.), ainsi que des langages formalisés de description ou de requêtes qui ne sont pas des langages de programmation.

L’importance de la spécification, de la documentation et des tests est à présenter, ainsi que l’intérêt de la modularisation qui permet la réutilisation de programmes et la mise à disposition de bibliothèques. Pour les programmes simples écrits par les élèves, on peut se contenter d’une spécification rapide mais précise.

---

Cette partie, entièrement consacrée à la programmation Python, abordera les points suivants :

1. Constructions élémentaires :
    - variable et affectation ;
    - séquence d'instructions ;
    - fonction ;
    - structures d'embranchement ;
    - structures de boucle (itératives et conditionnelles).

2. Types et valeurs de base :
    - entier (int) ;
    - flottant (float) ;
    - booléen (bool) ;
    - chaîne de caractères (str).

3. Types construits :
    - liste (list) ;
    - tuple (tuple) ;
    - dictionnaire (dict).

4. Traitements de données :
    - gestion de fichiers texte et csv ;
    - traitement de données en table.

De nombreux mini-projets et projets plus ambitieux jalonneront les diverses activités de prise en main de ces différentes notions.