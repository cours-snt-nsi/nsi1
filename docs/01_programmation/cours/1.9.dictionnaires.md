---
author: Steeve PYTEL
title: 🍋 Dictionnaires

hide:
    - toc
---

# Chapitre 9 : dictionnaires

![Dictionnaires](../../../assets/dictionnaire.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Hedy Lamarr](../../../assets/hedy.jpg){ style="display: block; margin: 0 auto" }

    !!! tip "Point Histoire"
        Hedwig Kiesler, dite [Hedy Lamarr](https://fr.wikipedia.org/wiki/Hedy_Lamarr){:target="_blank"}, est une actrice, productrice de cinéma et inventrice autrichienne, naturalisée américaine. Elle a co-développé un système de communication appelé « saut de fréquence » qui a jeté les bases de la technologie du spectre étalé, utilisée aujourd'hui dans les communications sans fil.

!!! danger ""

    Au chapitre 6, nous avons vu qu'en Python un tableau (au sens usuel du terme) est assimilé à une structure spécifique appelée *liste*.

    En informatique, il existe d'autres types de tableaux, comme les *tableaux associatifs* qui fonctionnent suivant des paires `clé:valeur`, la *clé* correspondant à une entrée du tableau et la *valeur* correspondant à la valeur associée. En Python, un tableau associatif est assimilé à une structure spécifique appelée *dictionnaire* (type `dict`).

    Le but de ce chapitre est de comprendre et manipuler ce nouveau type de donnée.

    On propose le déroulé suivant :

    1. un notebook que l'élève complètera en s'aidant du cours proposé ;
    2. un QCM d'auto-évaluation.

??? note "01. Introduction "

    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Dictionnaires.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

### Accès direct via Capytale

Dans la zone Rechercher, taper la phrase ci-dessous :

- [NSI Première Partie 1 Chapitre 9 Dictionnaires](https://capytale2.ac-paris.fr/web/c/c459-3801243){:target="_blank"}

### Téléchargement
[Téléchargement des ressources](https://nuage03.apps.education.fr/index.php/s/sceaHM2QPYKo64d){:target="_blank"}
