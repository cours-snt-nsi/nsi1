---
author: Claire Mathieu
title: 💡 Challenges

hide:
    - toc
---

# Challenges

![Challenges](../../../assets/challenges.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Claire Mathieu](../../../assets/claire.png){ style="display: block; margin: 0 auto" }

    !!! tip "Claire Mathieu"
        [Claire Mathieu](https://fr.wikipedia.org/wiki/Claire_Mathieu){:target="_blank"}, connue aussi sous le nom Claire Kenyon, née le 9 mars 1965, est une informaticienne et mathématicienne française, connue pour ses recherches sur les algorithmes d'approximation, les algorithmes en ligne, et la théorie des enchères. Elle travaille en tant que directrice de recherche au Centre National de la Recherche Scientifique (CNRS) et est chercheuse à l'Institut de recherche en informatique fondamentale.

On propose ci-dessous, uniquement en téléchargement et au format notebook séquencé, divers challenges en algorithmique et en programmation Python et/ou Javascript.

![Intentions](../../../assets/intentions.png){ style="display: block; margin: 0 auto" }

## Art Expo

Aidez le galeriste à accéder aux douze tableaux les plus célèbres du monde avant l’inauguration de l’exposition Expo Art (d'après une activité de [101computing.net](https://www.101computing.net/){:target="_blank"}).  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/FqM98He5pbRzNix){:target="_blank"} pour télécharger ce projet.

## Blackbeard’s Hidden Treasures

Découvrez les douze lieux où le célèbre pirate Barbe-Noire a caché son trésor (en anglais, d'après une activité de [101computing.net](https://www.101computing.net/){:target="_blank"}).  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/59oomymTCAKzebj){:target="_blank"} pour télécharger ce projet.

## Blackbeard’s Treasure Map

Partez à la recherche du trésor du célèbre pirate Barbe-Noire (en anglais, d'après une activité de [101computing.net](https://www.101computing.net/){:target="_blank"}).  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/Gk3Z5AojfT8sTJR){:target="_blank"} pour télécharger ce projet.

## Connexion administrateur

Faites-vous passer pour l'administrateur d'un système et connectez-vous sur son espace web dédié afin de récupérer son flag.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/Abg3mQ4wqsJCFnE){:target="_blank"} pour télécharger ce projet.

## Cryptage renforcé

Décodez le message secret caché à l'intérieur d'un dossier hautement crypté.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/NtLAj7tzzDLctbQ){:target="_blank"} pour télécharger ce projet.

## Dossier protégé

Décodez un message caché à l'intérieur d'un dossier protégé par un mot de passe.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/8eH4MaDe2TB9tgN){:target="_blank"} pour télécharger ce projet.

## Espionnage (d'après Passe ton hack d'abord)

Exécutez une mission secrète à haut risque.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/TGLHgHDkynsM2iD){:target="_blank"} pour télécharger ce projet.

## Football Premier League

Suivez d'au plus près le championnat de football de Premier League (en anglais, d'après une activité de [101computing.net](https://www.101computing.net/){:target="_blank"}).  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/67fYgpjcFtSjdfp){:target="_blank"} pour télécharger ce projet.

## Rois de France

Partez à la découverte de l'Histoire de France.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/sZrmcGtZ326KQbG){:target="_blank"} pour télécharger ce projet.
