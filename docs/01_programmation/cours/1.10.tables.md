---
author: Steeve PYTEL
title: 🍊 Données en table

hide:
    - toc
---

# Chapitre 10 : données en table

![Données en table](../../../assets/table.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Mary Kenneth Keller](../../../assets/soeur.jpg){ style="display: block; margin: 0 auto" }

    !!! tip "Point Histoire"
        [Mary Kenneth Keller](https://fr.wikipedia.org/wiki/Mary_Kenneth_Keller){:target="_blank}, sœur de la charité de la Bienheureuse Vierge Marie, B.V.M. (1913 – 1985), est une religieuse catholique américaine qui fut enseignante et pionnière dans les sciences informatiques. Elle est la première femme à obtenir un doctorat en informatique aux États-Unis. Elle est pionnière de l'intelligence artificielle, a cofondé l'AI Lab à l'Université Stanford et a contribué à la création du langage de programmation BASIC.

!!! danger ""

    Au chapitre 8, nous avons appris à manipuler les fichiers de type *texte brut* (extension `.txt`).

    Le but de ce chapitre est d'apprendre à manipuler un autre type de fichier : les fichiers csv.

    On propose le déroulé suivant :

    1. un notebook d'introduction à la manipulation des fichiers csv, faisant office de cours ;
    2. un notebook d'approfondissement et d'entraînement.

![Intentions](../../../assets/intentions.png){ style="display: block; margin: 0 auto" }

??? note "01. Introduction "

    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Dictionnaires.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

### Accès via Capytale

Dans la zone Rechercher, taper les phrases ci-dessous :

- [NSI Première Partie 1 Chapitre 10 Données en table 1/2](https://capytale2.ac-paris.fr/web/c/7248-3801244){:target="_blank"}
- [NSI Première Partie 1 Chapitre 10 Données en table 2/2](https://capytale2.ac-paris.fr/web/c/6196-3801245){:target="_blank"}

### Téléchargement
[Téléchargement des ressources](https://nuage03.apps.education.fr/index.php/s/xGgoH47QLw3Rxj3){:target="_blank"}
