---
author: Cynthia Breazeal
title: 🐍 Projets de programmation Python

hide:
    - toc
---

# Projets de programmation Python

![Projets de programmation Python](../../../assets/projet.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Cynthia Breazeal](../../../assets/cynthia.png){ style="display: block; margin: 0 auto" }

    !!! tip "Cynthia Breazeal"
        [Cynthia Breazeal](https://fr.wikipedia.org/wiki/Cynthia_Breazeal){:target="_blank"} est une professeur associée d'Arts des médias et Sciences au Massachusetts Institute of Technology (MIT), où elle dirige le Personal Robots Group du MIT Media Lab. Elle a créé le robot social Jibo et a contribué au développement de la robotique humanoïde.

---

## Jeu du morpion

![Jeu du morpion](../../../assets/morpion.png){ style="display: block; margin: 0 auto" }

Projet guidé au format notebook pour implémenter une version console du jeu du morpion.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/sH2KwEXTNQWzM46){:target="_blank"} pour télécharger ce projet.

---

## Jeu du pendu

![Jeu du pendu](../../../assets/pendu.png){ style="display: block; margin: 0 auto" }

Projet guidé au format notebook pour implémenter une version console du jeu du pendu. Une liste de mots à trouver est fournie sous la forme d'un fichier texte à joindre au notebook.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/Zms5Rjb2MF6dk5f){:target="_blank"} pour télécharger ce projet.

---

## Jeu découverte

![Jeu découverte](../../../assets/projet3.png){ style="display: block; margin: 0 auto" }

Projet d'initiation à la programmation événementielle nécessitant un EDI sur lequel est installée la bibliothèque `pygame`. Une bibliothèque `graphics_nsi` et sa documentation sont fournies. Les élèves apprennent les rudiments de la programmation événementielle à travers divers exercices liant interface graphique et interaction avec la souris et le clavier. Un mini-projet guidé est également proposé.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/MXj5M685dzi4NWn){:target="_blank"} pour télécharger ce projet.

---

## Traitement d'images

![Traitement d'images](../../../assets/traitement.png){ style="display: block; margin: 0 auto" }

Projet guidé au format notebook pour s'initier au traitement d'images. On présente les formats d'images en niveau de gris et on manipule en particulier les images PGM. Les images sont représentées sous la forme de listes de listes.  
Cliquer [ici](https://nuage03.apps.education.fr/index.php/s/XXT9g9xr4BE6Nex){:target="_blank"} pour télécharger ce projet.
