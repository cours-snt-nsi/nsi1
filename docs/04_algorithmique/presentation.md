---
author: Steeve PYTEL
title: 📖 Algorithmique
---

# Algorithmique


!!! abstract "Présentation"

    Le concept de méthode algorithmique est introduit ; de nouveaux exemples seront vus en terminale. Quelques algorithmes classiques sont étudiés. L’étude de leurs coûts respectifs prend tout son sens dans le cas de données nombreuses, qui peuvent être préférentiellement des données ouvertes.  
    Il est nécessaire de montrer l’intérêt de prouver la correction d’un algorithme pour lequel on dispose d’une spécification précise, notamment en mobilisant la notion d’invariant sur des exemples simples. La nécessité de prouver la terminaison d’un programme est mise en évidence dès qu’on utilise une boucle non bornée (ou, en terminale, des fonctions récursives) grâce à la mobilisation de la notion de variant sur des exemples simples.  

|Contenus|Capacités attendues|
|:--- | :--- |
|Parcours séquentiel d’un tableau|Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque. Écrire un algorithme de recherche d’un extremum, de calcul d’une moyenne.|
|Tris par insertion, par sélection|Écrire un algorithme de tri. Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection.|
|Algorithme des k plus proches voisinsn|Écrire un algorithme qui prédit la classe d’un élément en fonction de la classe majoritaire de ses k plus proches voisins.|
|Recherche dichotomique dans un tableau trié|Montrer la terminaison de la recherche dichotomique à l’aide d’un variant de boucle.|
|Algorithmes gloutons|Résoudre un problème grâce à un algorithme glouton.|