---
author: Steeve PYTEL
title: 📚 portes logiques

hide:
    - toc
---

# Chapitre 4 : portes logiques

![Picture](../../../assets/portes.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Picture](../../../assets/anca.png){ style="display: block; margin: 0 auto" }

    !!! tip "Point Histoire"
        [Anca Dragan](http://people.eecs.berkeley.edu/~anca/index.html){:target="_blank"}
        est professeure associée au département EECS de l'UC Berkeley. Son objectif est de permettre aux robots de travailler avec, autour et à l'appui des gens. Elle dirige le laboratoire InterACT, travaillant sur les algorithmes d'interaction homme-robot, des algorithmes qui vont au-delà de la fonction du robot de manière isolée et génèrent un comportement de robot qui se coordonne bien avec les gens.

!!! danger ""

    Au chapitre 3, nous nous sommes intéressés aux composants des ordinateurs dans l'architecture de von Neumann, ainsi qu'à leurs interactions.
    Tous ces composants sont en fait élaborés à partir de circuits électroniques plus ou moins complexes, eux-mêmes construits à partir d’un seul et même type de composant : le *transistor*.
    Ces différents circuits électroniques sont classés en différentes catégories : les *circuits logiques élémentaires*, également appelés *portes*, les *circuits combinatoires* et les *circuits séquentiels*.
    Le but de ce chapitre est de présenter quelques exemples de constructions de tels circuits.
    On propose le déroulé suivant :
    
    1. un point de cours (paragraphe 1) présentant le transistor ;
    2. un premier exercice (exercice 1) permettant de prendre en main le logiciel [Logisim](http://www.cburch.com/logisim/index.html) et de réaliser les trois portes `NON`, `ET` et `OU` ;
    3. un point de cours (paragraphe 2) pour faire un bilan écrit des trois portes réalisées dans l'exercice 1 ;
    4. trois exercices (exercices 2 à 4) permettant de réaliser à l'aide du logiciel [Logisim](http://www.cburch.com/logisim/index.html) les trois portes `NON ET`, `NON OU` et `OU EXCLUSIF` ;
    5. un point de cours (paragraphe 3) présentant deux exemples simples de circuits combinatoires : le multiplexeur et l'additionneur 1 bit ;
    6. un exercice (exercice 5) permettant de réaliser un additionneur multi-bits à l'aide du logiciel [Logisim](http://www.cburch.com/logisim/index.html) ;
    7. un exercice (exercice 6) traitant un circuit combinatoire permettant de réaliser la fonction booléenne `EQUIVALENT A`.

??? note "01. Cours"

    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Portes_logiques_cours.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

??? note "02. Exercices"

    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Portes_logiques_cours_exercices.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

### Accès via Capytale

Pas de ressources à télécharger pour ce chapitre.

---
