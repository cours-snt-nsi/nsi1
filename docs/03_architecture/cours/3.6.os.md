---

author: Steeve PYTEL
title: 📚 systèmes d'exploitation

hide:
    - toc
---

# Chapitre 6 : systèmes d'exploitation

![Picture](../../../assets/systeme.png){ style="display: block; margin: 0 auto" }

!!! note ""
    ![Picture](../../../assets/Margaret.jpg){ style="display: block; margin: 0 auto" }

    !!! tip "Point Histoire"
        [Margaret Hamilton](https://fr.wikipedia.org/wiki/Margaret_Hamilton_(scientifique)){:target="_blank"} est une informaticienne, ingénieure système et cheffe d'entreprise américaine. Elle était directrice du département génie logiciel (« software engineering », terme de son invention) au sein du MIT Instrumentation Laboratory qui conçut le système embarqué du programme spatial Apollo. En 1986, elle fonde la société Hamilton Technologies, Inc. à partir de ses travaux entrepris au MIT.

!!! danger ""

    Le but de ce chapitre est de présenter, à travers un TP en autonomie, quelques éléments fondamentaux des systèmes d'exploitation.
    
    On propose le déroulé suivant :
    
    1. Un TP présentant quelques notions clés des systèmes d'exploitation (TP OS).

??? note "01. TP (OS)"

    <div style="text-align: center;">
        <iframe 
            src="../../../documents/Systeme_exploitation_cours_activite_introduction.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

### Accès via Capytale

Pas de ressources à télécharger pour ce chapitre.

---