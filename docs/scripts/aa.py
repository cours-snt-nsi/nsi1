# --- PYODIDE:env --- #
# Import de matplotlib (installation lors du 1er lancement)
import networkx as nx
import matplotlib.pyplot as plt
import random


# Précision du backend à utiliser
matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

# Insertion de la courbe dans une div spécifiée (id="cible_1")
from js import document 
document.pyodideMplTarget = document.getElementById("cible_1")
# On vide la div
document.getElementById("cible_1").textContent = ""

# --- PYODIDE:code --- #


plt.clf()
# Créer un nouveau graphe non dirigé
G = nx.Graph()

# Ajouter des nœuds au graphe
nœuds = ['A', 'B', 'C', 'D', 'E', 'F']
G.add_nodes_from(nœuds)

# Ajouter des arêtes avec des poids aléatoires entre 1 et 5
arêtes = [('A', 'B'), ('A', 'C'), ('B', 'C'), ('C', 'D'), ('D', 'E'), ('E', 'F'), ('F', 'A')]
for u, v in arêtes:
    G.add_edge(u, v, poids=random.randint(1, 5))

# Dessiner le graphe avec les poids des arêtes
pos = nx.spring_layout(G)  # positions for all nodes
nx.draw(G, pos, with_labels=True, node_size=700, node_color="lightblue", edge_color="gray")

# labels des poids
edge_labels = nx.get_edge_attributes(G, 'poids')
nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)

plt.show()
