title: tutoriels

# Introductions vers de nouveaux langages/modules

Par Adrien Willm : découvrir [javascript](http://numerique.ostralo.net/javascriptapprentissage/){target="_blank"}  et de petits [exemples](http://numerique.ostralo.net/javascriptexemples/){target="_blank"}

[Casse Brique 2D en Javascript](https://developer.mozilla.org/fr/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript){target="_blank"}

[Site Grafikart](https://grafikart.fr/){target="_blank"} tutoriels vidéos gratuits sur SQL, Javascript, PHP...

[Site découvrir pygame](https://victorminator.github.io/atelier_pygame/){target="_blank"} apprendre à utiliser le module Pygame par un élève de la réunion

[Site découvrir le dessin](https://nreveret.forge.aeif.fr/dessins/){target="_blank"} découvrir la programmation en réalisant des dessins avec Python

