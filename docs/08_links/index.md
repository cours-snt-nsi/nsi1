title: Accueil

Ressources pour les enseigants de NSI (Numérique et Sciences Informatiques)



![NSI](images/logo-forum.png){width=4%}  [Forum Numérique et Sciences Informatiques](https://mooc-forums.inria.fr/moocnsi/){target="_blank"} 

![NSI](images/forge.png){width=5%} [Forge des communs numériques éducatifs ](https://docs.forge.apps.education.fr/){target="_blank"} 


![NSI](images/logo_aeif_300.png){width=5%}  [AEIF Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/){target="_blank"} 

![NSI](images/logo-sif-2022.svg){width=5%}  [SIF Société Informatique de France](https://www.societe-informatique-de-france.fr/){target="_blank"}

![NSI](images/source.jpg)


