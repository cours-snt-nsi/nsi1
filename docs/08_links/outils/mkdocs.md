title: mkdocs

# Mkdocs

MkDocs permet de générer de la documentation autour de l'écosystème Markdown et Python.

???info "Pyodide-Mkdocs-Theme : Éditeurs & terminaux python dans MkDocs de Frederic Zinelli"
    === "Site"
        [Site](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){target="_blank"}
    === "Repository"
        [site]( https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme){target="_blank"}
   


???info "Tutoriels et aides pour l'enseignant qui crée son site de Mireille Coilhac"
    [Site](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){target="_blank"}


???info "Travaux de Franck Chambon"
    [site](https://fchambon.forge.apps.education.fr/classe/8-productions/#2020-2022-travail-avec-mkdocs){target="_blank"}
    

???info "ADN tutoriel de Eric Madec"
    [site](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/){target="_blank"}


???info "Tutoriel vidéo de Fred Leleu"
    [Vidéo](https://www.youtube.com/playlist?list=PL-Q7fIakgvUAcUluPeUMIP1128wWxboJY){target="_blank"}




???info "Terminal et IDE Python dans mkdocs : Pyodide, de Vincent Bouillot"
    [Site](https://bouillotvincent.gitlab.io/pyodide-mkdocs/){target="_blank"}



- [Site mkdocs](https://www.mkdocs.org/){target="_blank"} Projet mkdocs : static site generator

- [Documentation Material for mkdocs](https://squidfunk.github.io/mkdocs-material/){target="_blank"} documentation framework on top of MkDocs

- [mkdocs-addresses](https://pypi.org/project/mkdocs-addresses/){target="_blank"} mkdocs automatic paths/addresses building - auto-completion support

- [Jupyter notebooks avec mkdocs](https://pypi.org/project/mkdocs-jupyter/){target="_blank"} Jupyter Notebooks in mkdocs

- [Graphvitz avec mkdocs](https://pypi.org/project/mkdocs-graphviz/){target="_blank"} render Graphviz graphs in Mkdocs, as inline SVGs and PNGs de Rodrigo Schwencke

- [Mkdocs-SQLite-Console](https://epithumia.github.io/mkdocs-sqlite-console/){target="_blank"} permet d'afficher un IDE SQL (SQLite) permettant d'exécuter du code de Rafaël Lopez


- [Diagrammes Kroki avec mkdocs](https://pypi.org/project/mkdocs-kroki-plugin/) et [exemples](https://kroki.io/examples.html){target="_blank"} plugin to embed Kroki-Diagrams into your documentation


- [PyMdwon Extentions](https://facelessuser.github.io/pymdown-extensions/){target="_blank"}  a collection of extensions for Python Markdown



- [Data tables](https://squidfunk.github.io/mkdocs-material/reference/data-tables/){target="_blank"} customisation des tableaux

