---
author: Steeve PYTEL
title: 📚 Bibliographie

hide:
    - toc
---

# Bibliographie et sitographie

![Bibliographie](../../../assets/biblio.png){ style="display: block; margin: 0 auto" }

---

## Bibliographie

  - **Algorithmique :**
    - [G. Dowek, *Introduction à la science informatique*, RPA, Scérén, CNDP-CRDP (2011)](https://www.eyrolles.com/Informatique/Livre/introduction-a-la-science-informatique-9782866311889/)
    - [T. H. Cormen, C. Leiserson, R. Rivest, C. Stein, *Algorithmique*, 3ème édition, Sciences Sup, Dunod (2010)](https://www.dunod.com/sciences-techniques/algorithmique-cours-avec-957-exercices-et-158-problemes/)

  - **Architecture des ordinateurs :**
    - [G. Dowek, *Introduction à la science informatique*, RPA, Scérén, CNDP-CRDP (2011)](https://www.eyrolles.com/Informatique/Livre/introduction-a-la-science-informatique-9782866311889/)
    - [G. Dowek et al., *Informatique et sciences du numérique, Spécialité ISN en terminale S*, Eyrolles (2012)](https://www.eyrolles.com/Informatique/Livre/informatique-et-sciences-du-numerique-specialite-isn-en-terminale-s-avec-des-ex-9782212135435/)
    - [C. Timsit et S. Zertal, *From transistor to computer: an introduction to the world of computer architecture*, Hermann (2013)](https://www.editions-hermann.fr/livre/from-transistor-to-computer-claude-timsit/)

  - **Programmation Python :**
    - [A. Casamayou-Boucau, P. Chauvin et G. Connan, *Programmation en Python pour les mathématiques*, 2ème édition, Dunod (2016)](https://www.dunod.com/sciences-techniques/programmation-en-python-pour-mathematiques-0/)
    - [G. Dowek et al., *Informatique et sciences du numérique, Spécialité ISN en terminale S*, Eyrolles (2012)](https://www.eyrolles.com/Informatique/Livre/informatique-et-sciences-du-numerique-specialite-isn-en-terminale-s-avec-des-ex-9782212135435/)
    - [G. Swinnen, *Apprendre à programmer avec Python 3* (en ligne)](http://inforef.be/swi/python.htm)

  - **Livres essentiels :**
    - [Les oubliées du numérique, Editeur Le Passeur (2019)](https://www.le-passeur-editeur.com/les-livres/essais/les-oubli%C3%A9es-du-num%C3%A9rique/)
    - [Les décodeuses du numérique](https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique/)

---

## Sitographie

- **Architecture des ordinateurs :**
    - **Instructions en ligne de commande :**
        - [Jeu Terminus](http://luffah.xyz/bidules/Terminus/)
        - [Machine virtuelle Linux](https://bellard.org/jslinux/)

    - **Simulation de portes logiques :**
        - [Logic](https://logic.modulo-info.ch/)
        - [Logisim](http://www.cburch.com/logisim/index.html)

- **Développement Web :**
    - **Cours :**
        - [OpenClassrooms (HTML5 et CSS3)](https://openclassrooms.com/fr/courses/1603881-creez-votre-site-web-avec-html5-et-css3/)
        - [OpenClassrooms (JavaScript)](https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript/)
        - [OpenClassrooms (PHP)](https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/)
        - [w3schools](https://w3schools.com/)

    - **Sites officiels :**
        - [Développeurs de Mozilla](https://developer.mozilla.org/fr/)
        - [JavaScript](https://www.javascript.com/)
        - [PHP](http://www.php.net/)
        - [W3C](https://www.w3.org/)

    - **Validateurs :**
        - [CSS](https://jigsaw.w3.org/css-validator/#validate_by_upload)
        - [HTML](https://validator.w3.org/#validate_by_upload+with_options)

- **Programmation Python :**
    - **Environnement de Développement Intégré (EDI) :**
        - [Anaconda (multiplateforme)](https://www.anaconda.com/download/)
        - [Basthon (en ligne)](https://console.basthon.fr/)
        - [EduPyter (windows)](https://www.edupyter.net/)
        - [EduPython (windows)](https://edupython.tuxfamily.org/)
        - [IDLE (multiplateforme)](https://www.python.org/downloads/)
        - [Spider (multiplateforme)](https://www.spyder-ide.org/)

    - **Gestion des notebooks :**
        - [Basthon](https://notebook.basthon.fr/)
        - [Capytale](https://capytale2.ac-paris.fr/web/c-auth/list/)

    - **Sites officiels :**
        - [OpenClassrooms](https://openclassrooms.com/courses/apprenez-a-programmer-en-python/)
        - [Pygame](https://www.pygame.org/news/)
        - [Python](https://www.python.org/doc/)
        - [w3schools](https://w3schools.com/)

- **Réseaux :**
    - **Simulation de réseaux :**
        - [Filius (site en allemand, mais possibilité de mettre le logiciel en français)](https://www.lernsoftware-filius.de/Herunterladen/)

- **Sites de cours de première :**
    - [France IOI : contenus gratuits pour découvrir la programmation, progresser en algorithmique et développer le goût d'apprendre et de réfléchir](https://www.france-ioi.org/)
    - [Jeux pour apprendre à programmer](https://blog.adatechschool.fr/jeux-apprendre-a-coder/)
    - [Jeu pour découvrir les bases du langage Python](https://py-rates.org/)
    - [Site de David Roche](https://pixees.fr/informatiquelycee/)
    - [Site de Gilles Lassus](https://glassus.github.io/premiere_nsi/)

- **Sites sur les sciences du numérique :**
    - [Algorithmes, logiciels, modèles, données... entrez dans les interstices](https://interstices.info/)
    - [INRIA](https://www.inria.fr/fr/)

