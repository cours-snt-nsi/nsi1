title: Cybersécurité

## Sites officiels

- [ANSSI](https://cyber.gouv.fr/){target="_blank"} Agence nationale de la sécurité des systèmes d'information : l'autorité nationale en matière de cybersécurité ; sa mission est de comprendre, prévenir et répondre au risque cyber

- [Eduscol](https://eduscol.education.fr/3679/education-et-cybersecurite){target="_blank"} éducation et cybersécurité 


- [Demain spécialiste cyber](https://www.demainspecialistecyber.fr/){target="_blank"} découvrez la cybersécurité et ses métiers

## Formations

- [Séminaire](https://podeduc.apps.education.fr/menj-cybersecurite-et-education/20221013-seminaire-de-formation-cybersecurite-specialite-nsi-campus-cyber/){target="_blank"}  formation au Campus Cyber, organisé par le MENJ (ministère de l'Éducation nationale et de la Jeunesse) avec les partenaires de la filière cybersécurité pour la spécialité NSI

- [SecNum](https://secnumacademie.gouv.fr/){target="_blank"} MOOC de l'ANSSI :  des informations pour vous initier à la cybersécurité, approfondir vos connaissances, et ainsi agir efficacement sur la protection de vos outils numériques

- [France Num](https://www.francenum.gouv.fr/formations/cybersecurite-formation-en-ligne-gratuite-pour-tous){target="_blank"} formation en ligne gratuite pour tous

- [Root Me](https://www.root-me.org/?lang=fr){target="_blank"} Une plateforme rapide, accessible et réaliste pour tester vos compétences en hacking


## CTF

 Découvrir la cybersécurité à travers les CTF (Challenges "capture the flag")

- [Challenges-Kids](https://www.challenges-kids.fr/){target="_blank"} une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands

- [Cyberini](https://cyberini.com/ctf/#crackmeCM){target="_blank"} CTF pour découvrir les différentes catégories et [vidéo](https://www.youtube.com/watch?v=ExBQwagrHwA){target="_blank"} d'introduction

- [Hackropole](https://hackropole.fr/fr/){target="_blank"} on vous propose de rejouer les épreuves du France Cybersecurity Challenge dans le but de découvrir et de vous former à divers domaines de la cybersécurité  (avec des propositions de solutions)



## En Anglais

- [Try hack Me](https://tryhackme.com/){target="_blank"} 

- [Hack The Box](https://www.hackthebox.com/){target="_blank"}

## Sites pratiques


- [Metadata 2 Go](https://www.metadata2go.com/){target="_blank"} Get All Metadata Info Of Your Files

- [Internet Archive](https://archive.org/){target="_blank"} Internet Archive is a non-profit library of millions of free books, movies, software, music, websites, and more.

- [Hashes](https://hashes.com/en/decrypt/hash){target="_blank"} Decrypt MD5, SHA1, MySQL, NTLM, SHA256, MD5 Email, SHA256 Email, SHA512 hashes

- [DCode](https://www.dcode.fr/){target="_blank"} un ensemble de plus de 900 outils pour aider à la résolution de jeux, énigmes, messages codés, mathématiques...


- [Data URL](https://base64.guru/tools/data-url-to-image){target="_blank"} convert data URL to image ; [Mozilla Documentation](https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/Data_URLs){target="_blank"} les URLs de données permettent aux créateurs de contenu d'intégrer de petits fichiers dans des documents

- [Tineye](https://tineye.com/){target="_blank"} reverse Image Search

- [Bing](https://www.bing.com/images/feed?form=HDRSC2){target="_blank"} recherche à l'aide d'une image

- [UserSearch](https://usersearch.org/index.php){target="_blank"} find someone by username, email, phone number or picture across Social Networks, Dating Sites, Forums, Crypto Forums, Chat Sites and Blogs

- [Whats My Name](https://whatsmyname.app/){target="_blank"} enumerate usernames across many websites

- [namech_k](https://namechk.com/){target="_blank"} check the availability of a username or domain name 