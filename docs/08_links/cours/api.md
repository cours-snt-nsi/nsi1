title: API

# API - Interfaces de Programmation Applicatives



Un ensemble normalisé de briques logicielles grâce auxquelles un système informatique offre des services à d’autres systèmes.

- [Listes d'API publiques](https://github.com/public-apis/public-apis){target="_blank"}

- - - - 

- [Data.gouv](https://www.data.gouv.fr/fr/){target="_blank"} Plateforme ouverte des données publiques françaises





- [API Peertube](https://docs.joinpeertube.org/api-rest-reference.html){target="_blank"}  library for your programming language to use PeerTube

- [Open Notify](http://open-notify.org/){target="_blank"} a simple programming interface for some of NASA’s awesome data

- [API nasa](https://api.nasa.gov/){target="_blank"}

- [The Space Devs](https://thespacedevs.com/){target="_blank"}



- [musicbrainz](https://musicbrainz.org/){target="_blank"} une encyclopédie musicale libre qui collecte des métadonnées musicales et les propose au public
 
- [Weather API](https://openweathermap.org/api){target="_blank"} Weather data


- [Dog API](https://dog.ceo/dog-api/){target="_blank"} he internet's biggest collection of open source dog pictures

- [OpenAI API](https://platform.openai.com/docs/overview){target="_blank"}  créer des images artificielles basées sur un grand réseau neuronal d'IA 

- [API Carto](https://apicarto.ign.fr/api/doc/){target="_blank"}
