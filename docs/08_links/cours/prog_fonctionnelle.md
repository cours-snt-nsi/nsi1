title: Programmation Fonctionnelle

[Formation](https://mermet.users.greyc.fr/Formations/FormationProgrammationFonctionnelle/correction.html){target="_blank"} à destination des enseignants de NSI sur la programmation fonctionnelle par Bruno Mermet

[Lambda Calcul](https://romainjanvier.forge.aeif.fr/nsiterminale/4_langage_programmation/4_lambda_calcul/1_lambda_calcul/){target="_blank"} un cours de Tle  par Romain Janvier

[Terminal](http://nsiterminale.janviercommelemois.fr/lambda_calcul/){target="_blank"} un terminal de lambda calcul  par Romain Janvier et Nicolas Reveret
 
[Coconut](https://coconut-lang.org/){target="_blank"} simple, elegant, Pythonic functional programming ; une surcouche de Python pour faciliter la programmation fonctionnelle