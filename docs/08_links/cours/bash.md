title: langage bash

# Langage Bash

- Simulateur Terminal Linux

[Site de l'université de la Réunion](https://moocbash.univ-reunion.fr/?cpu=asm&n=1){target="_blank"}

 [Site jslinux de Fabrice Bellard](https://bellard.org/jslinux/vm.html?cpu=riscv64&url=fedora33-riscv.cfg&mem=512){target="_blank"} ou [JSLinux](http://jslinux.org/){target="_blank"}

[Weblinux de Thomas Castanet](https://chinginfo.fr/dossier/weblinux/?user=wvn2i8u2Ib&cpu=asm&n=1&relayURL=wss%3A%2F%2Frelay.widgetry.org%2F&refresh=okok){target="_blank"}

- - - - - - - - - - - - - - - - - - - - - - - -

- Lien vers le MOOC « Maîtriser le shell Bash » disponible sur la plateforme de FUN (France Université Numérique):

[MOOC](https://www.fun-mooc.fr/fr/cours/maitriser-le-shell-bash/){target="_blank"}

- - - - - - - - - - - - - - - - - - - - - - - -

- Documentation

[Les hirondelles du net](https://www.leshirondellesdunet.com/bash){target="_blank"}

Quentin Busuttil [101 commandes indispensables sous Linux](https://buzut.net/101-commandes-indispensables-sous-linux/){target="_blank"}

Mendel Cooper [Guide avancé d'écriture des scripts Bash](https://abs.traduc.org/abs-fr/index.html){target="_blank"}

- - - - - - - - - - - - - - - - - - - - - - - -

[Jeux Terminus](http://luffah.xyz/bidules/Terminus/){target="_blank"}

[Jeu GameShell](https://github.com/phyver/GameShell){target="_blank"} Repository de Pierre Hyvernat et Rodolphe Lepigre.

[Jeu bandit](https://overthewire.org/wargames/bandit/bandit0.html){target="_blank"} Découvrir le langage Bash




