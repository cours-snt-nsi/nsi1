---
author: Steeve PYTEL
title: 📖 Réseaux
---

# Les réseaux


!!! abstract "Présentation"

    Les réseaux informatiques permettent de transmettre des informations entre différentes machines (ordinateurs, serveurs, imprimantes, téléphones, voitures, etc.) à travers divers types de connexion (filaire, sans-fil, optioque).  
    Ces transmissions sont réalisées à partir de règles de communication appelées <span style='font-style:italic;'>protocoles</span>.  
    Chaque machine est repérée sur le réseau à partir d'une adresse unique appelée <span style='font-style:italic;'>adresse IP</span>.


 

!!! alerte "Cette partie, entièrement consacrée aux réseaux, abordera les points suivants :"
    1. Réseaux :
        - histoire et évolution ;
        - protocoles ;
        - éléments physiques ;
        - modèle TCP/IP ;
        - couches réseaux ;
        - adresse IP et notation CIDR.



    2. Bit alterné :
        - principe général du protocole du bit alterné ;
        - minuteur, acquittement et duplicata.

