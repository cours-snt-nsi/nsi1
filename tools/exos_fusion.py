from pathlib import Path

TARGET = 'docs'
MERGED_ROOT = Path('./fusion_tmp')

for file in MERGED_ROOT.glob("**/*.py"):
    print('Handle:', file)
    rel = file.relative_to(MERGED_ROOT)

    target = TARGET / rel
    target.write_bytes(file.read_bytes())

    stem = target.stem
    for suffix in ('_test.py', '_corr.py'):
        sibling = target.with_name( stem+suffix )
        if sibling.is_file():
            sibling.unlink()
