'''
Script renaming all the sujet.md files into index.md files
'''


import os
from pathlib import Path



TARGET = Path('docs')
HOME = TARGET / "index.md"

FROM, TO = "sujet.md index.md".split()

for file in TARGET.rglob(FROM):
    if file != HOME:
        cmd = f'''git mv { file } { file.parent / TO }'''
        print(cmd)
        os.system(cmd)
